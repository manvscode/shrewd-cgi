shrewd-cgi
==========

Since the earliest days of the Internet and the hypertext transfer protocol (HTTP), web servers were efficient at serving static files that never changed. However, the modern landscape of web applications have revealed the occasional need for web applications and web services to handle requests that may be CPU intensive or require extreme specialization to squeeze out every CPU cycle possible.

Shrewd Creative have developed a FastCGI framework for developing high performance web applications and services in C/C++. With the Shrewd CGI framework, developers can develop high performance FastCGI applications that have many of the facilities found in server side programming technologies such as PHP, ASP.Net, et cetera, with the exception of being light-weight and customizable.

Developers will be able to quickly develop web applications in C or C++ as they would in PHP or ASP.Net.
