/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _MIME_H_
#define _MIME_H_

#include <types.h>

#ifdef _DEBUG_MIME
#define _DEBUG_VECTOR
#endif
#include <vector.h>

typedef struct tagMimeRecord {
	char *mime_type;
	char *extension;
} MimeRecord;

typedef vector MimeTable; /* table of mime records */

boolean     mime_create           ( MimeTable *p_table );
boolean     mime_create_from_file ( MimeTable *p_table, const char *s_mime_file );
void        mime_destroy          ( MimeTable *p_table );
void        mime_debug_table      ( const MimeTable *p_table );
const char* mime_type             ( const MimeTable *p_table, const char *extension );
int         mime_record_compare   ( const void *a, const void *b );



#endif
