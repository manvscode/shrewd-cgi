%{
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
%}

%union {
   char     *lexeme;
   }

%token <lexeme>TOKEN <lexeme>SCONST 

%type <lexeme> Tokens

%left '='

%%

Statements: Statement                            
        |   Statements Statement
        ;


Statement:  TOKEN '=' Tokens '\n'   { printf("%s: %s\n", $1, $3); free($1); free($3);}
        |   TOKEN '=' SCONST '\n'   { printf("%s: %s\n", $1, $3); free($1); free($3);}
        ;

Tokens:     TOKEN                   { 
                                        $$ = $1; 
                                    }
        |   Tokens TOKEN            { 
                                        int l = strlen($1) + strlen($2); 
                                        char *s = malloc(l + 2); 
                                        strcpy(s,$1); 
                                        strcat(s,$2); 
                                        $$ = s; 
                                        free($1); 
                                        free($2);
                                    }
        ;
%%

#ifdef DEBUG        
extern void SetYYin( FILE *fp );
void main(void)
{
    SetYYin( stdin );
    yyparse();
}
#endif