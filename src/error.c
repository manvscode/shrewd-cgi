/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifdef FCGI_BUILD
#include <fcgi_stdio.h> /* fcgi library; put it first*/
#endif
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <assert.h>
#include <syslog.h>

#include "cgi.h"

const char *cgi_error_types[] = {
	"CGI Warning",
	"CGI Fatal",
	"CGI Caution",
	"CGI Information",
	"CGI out of memory"
};

void cgi_error( cgi_error_code error_code, const char *format, ... )
{
	va_list arguments;

	if( cgi_is_enabled(CGI_OPT_DISPLAY_ERRORS, cgi.options) ) 
	{
		char buffer[ 128 ];
		va_start( arguments, format );
		/*
		fprintf( stderr, "<b> %s </b>: ", cgi_error_types[ error_code ] );
		vfprintf( stderr, format, arguments );
		fprintf( stderr, "<br />" );
		*/
		vsnprintf( buffer, sizeof(buffer), format, arguments );
		cgi_out( "<b> %s </b>: %s <br />", cgi_error_types[ error_code ], buffer );
		va_end( arguments );
	}

	va_start( arguments, format );
	cgi_vlog( error_code, format, arguments );
	va_end( arguments );

	switch( error_code )
	{
		case E_FATAL: /* fall through */
		case E_MEMORY:
			cgi_destroy( );
			exit( EXIT_FAILURE );
		default:
			break;
	}
}
