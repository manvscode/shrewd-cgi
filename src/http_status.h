/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _HTTP_STATUS_H_
#define _HTTP_STATUS_H_

typedef struct struct_http_status {
	int   status_code;
	char* reason_phrase;
	char* long_description;
} http_status;

#define HTTP_STATUS_TABLE_SIZE   (55)

extern http_status http_status_table[ HTTP_STATUS_TABLE_SIZE ];


#define     http_status_table_initialize( ) (qsort( http_status_table, HTTP_STATUS_TABLE_SIZE, sizeof(http_status), http_status_compare ))
const char* http_status_reason( int code );
const char* http_status_long_description( int code );
int         http_status_compare( const void *left, const void *right );

#endif /* _HTTP_STATUS_H_ */
