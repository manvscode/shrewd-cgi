/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifdef FCGI_BUILD
#include <fcgi_stdio.h> /* fcgi library; put it first*/
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <libcollections/hash-functions.h>
#include <libcollections/tstring.h>
#include "cgi.h"
#include "http_status.h"
#include "url.h"
#include "string_aux.h"
#include "multipart.h"


#define CGI_EMPTY_STRING                   ("")
#define CGI_ZERO_STRING                    ("0")
#define CGI_CRLR                           ("\r\n")
#define CGI_CRLR_LENGTH                    (sizeof(CGI_CRLR) - 1)
#define CGI_FORM_URL_ENCODED               ("application/x-www-form-urlencoded")
#define CGI_FORM_URL_ENCODED_LENGTH        (sizeof(CGI_FORM_URL_ENCODED) - 1)
#define CGI_MULTIPART_FORM_DATA            ("multipart/form-data")
#define CGI_MULTIPART_FORM_DATA_LENGTH     (sizeof(CGI_MULTIPART_FORM_DATA) - 1)
#define CGI_DEFAULT_PROTOCOL               ("HTTP/1.0")



static void    _cgi_process_query_string        ( char *query_string, hash_map_t *p_map );
static void    _cgi_process_multipart_form_data ( char *post_buffer, size_t content_length, const char *boundary, hash_map_t *p_map );
static void    cgi_send_headers                 ( );

void           cgi_log_initialize   ( );
void           cgi_log_deinitialize ( );
static boolean key_value_destroy    ( void *key, void *value );
static int     key_compare          ( const void *left, const void *right );
#if defined(HEADERS_HASHED)
static size_t  header_hash          ( const void *header );
#endif
static boolean header_destroy       ( void *header );
static int     header_compare       ( const void *left, const void *right );
static size_t  cgi_file_hash        ( const void *file );
static boolean cgi_file_destroy     ( void *file );
static int     cgi_file_compare     ( const void *left, const void *right );



extern boolean     cgi_session_initialize   ( void );
extern boolean     cgi_session_deinitialize ( void );
extern void        cgi_session_start        ( void );
extern void        cgi_session_end          ( void );

cgi_handle cgi;


/*
 *  Main CGI Functions
 */
void cgi_create( flags_t f, const char *application_name, const char *domain )
{
	#ifdef _DEBUG_CGI
	memset( &cgi, 0, sizeof(cgi) );
	cgi_set_bit( CGI_OPT_DISPLAY_ERRORS, cgi.options );
	cgi_set_bit( CGI_OPT_ENABLE_LOGGING, cgi.options );
	#endif

	assert( application_name );
	assert( *application_name );
	
	strncpy( cgi.application_name, application_name, sizeof(cgi.application_name) );
	cgi.domain            = strdup( domain );
	cgi.options           = f;
	cgi.request_method    = UNDEFINED;
	cgi.state             = 0;


	#ifdef NO_PROCESS_HEADERS
	http_status_table_initialize( );
	#endif

	#ifdef HEADERS_HASHED
	hash_table_create( &cgi.response_headers, CGI_HEADER_TBLSIZE, header_hash, header_destroy, header_compare, malloc, free );
	#else
	rbtree_create( &cgi.response_headers, header_destroy, header_compare, malloc, free );
	#endif
	hash_map_create( &cgi.get_variables, CGI_PARAM_TBLSIZE, string_hash, key_value_destroy, key_compare, malloc, free );
	hash_map_create( &cgi.post_variables, CGI_PARAM_TBLSIZE, string_hash, key_value_destroy, key_compare, malloc, free );
	hash_map_create( &cgi.cookies, CGI_COOKIE_TBLSIZE, string_hash, key_value_destroy, key_compare, malloc, free );
	hash_table_create( &cgi.files, CGI_FILE_TBLSIZE, cgi_file_hash, cgi_file_destroy, cgi_file_compare, malloc, free );

	if( cgi_is_enabled(CGI_OPT_ENABLE_SESSIONS, cgi.options) && !cgi_session_initialize( ) )
	{
		cgi_error( E_FATAL, "Cannot initialize session.\n" );
	}

	buffer_create( &cgi.output_buffer, CGI_OUTPUT_BUFFER_SIZE );

	cgi_log_initialize( );
	cgi_log( E_INFO, "CGI Created\n" );
}

void cgi_destroy( )
{
	free( cgi.domain );

	#ifdef HEADERS_HASHED
	hash_table_destroy( &cgi.response_headers );
	#else
	rbtree_destroy( &cgi.response_headers );
	#endif
	hash_map_destroy( &cgi.get_variables );
	hash_map_destroy( &cgi.post_variables );
	hash_map_destroy( &cgi.cookies );
	hash_table_destroy( &cgi.files );

	if( cgi_is_enabled(CGI_OPT_ENABLE_SESSIONS, cgi.options) && !cgi_session_deinitialize( ) )
	{
		cgi_error( E_FATAL, "Cannot deinitialize session.\n" );
	}

	buffer_destroy( &cgi.output_buffer );

	cgi_log( E_INFO, "CGI Destroyed\n" );
	cgi_log_deinitialize( );

	#ifdef _DEBUG_CGI
	cgi.request_method = UNDEFINED;
	cgi.state          = 0;
	cgi.options        = 0;
	#endif
}

const char* cgi_app( )
{
	return cgi.application_name;
}

const char* cgi_domain( )
{
	return cgi.domain;
}

void cgi_reset( )
{
	if( cgi_is_enabled(CGI_OPT_ENABLE_SESSIONS, cgi.options) )
	{
		cgi_session_end( );	
	}

	#ifdef HEADERS_HASHED
	hash_table_clear( &cgi.response_headers );
	#else
	rbtree_clear( &cgi.response_headers );
	#endif
	hash_map_clear( &cgi.get_variables );
	hash_map_clear( &cgi.post_variables );
	hash_map_clear( &cgi.cookies );
	hash_table_clear( &cgi.files );
	buffer_clear( &cgi.output_buffer );

	cgi.request_method = UNDEFINED;
	cgi.session        = NULL;
	cgi.state          = 0;
}

void _cgi_process_query_string( char *query_string, hash_map_t *p_map )
{
	const char *delimiters = "&";
	char *token            = strtok( query_string, delimiters );

	while( token != NULL ) 
	{
		char *equal_sign_index  = strchr( token, '=' );

		/* Allocate memory for key and value strings */
		*equal_sign_index = '\0';
		char *key         = strdup( token );
		char *value       = strdup( equal_sign_index + 1 );

		url_decode( key );
		url_decode( value );

		hash_map_insert( p_map, key, value );

		token = strtok( NULL, delimiters );
	}	
}

#define cgi_fill_post_buffer(file_stream, request_content_length) \
	{ \
		size_t bytes_read = 0L; \
		while( bytes_read < (request_content_length) ) \
		{ \
			bytes_read += fread( cgi.post_buffer + bytes_read, sizeof(char), sizeof(cgi.post_buffer) - bytes_read, file_stream ); \
		} \
		*(cgi.post_buffer + bytes_read) = '\0'; \
		assert( bytes_read == request_content_length ); \
	}

void _cgi_process_multipart_form_data( char *post_buffer, size_t content_length, const char *boundary, hash_map_t *p_map )
{
	multipart_lexer lexer;
	multipart_parser parser;

	multipart_lexer_create( &lexer, (const char*) post_buffer, content_length, boundary );
	multipart_lexer_advance( &lexer );

	while( lexer.token.type != TOK_END && lexer.token.type != TOK_BOUNDARY_END ) 
	{
		multipart_parser_handle_part( &parser, &lexer );

		if( parser.filename.type != TOK_UNDEFINED )	 /* file posted variable */
		{ 
			cgi_file *p_file = (cgi_file *) malloc( sizeof(cgi_file) );

			if( !p_file) 
			{
				cgi_error( E_MEMORY, "Unable to allocate memory. (%s:%d)", __FILE__, __LINE__ );
			}

			if( parser.name.type != TOK_UNDEFINED )	
			{
				p_file->name = strndup( parser.name.lexeme, parser.name.length );

				if( !p_file->name ) 
				{
					free( p_file );
					multipart_lexer_destroy( &lexer );
					cgi_error( E_MEMORY, "Unable to allocate memory. (%s:%d)", __FILE__, __LINE__ );
				}
				trim( p_file->name, "\n\r\t " );
			}
			else
			{
				free( p_file );
				multipart_lexer_destroy( &lexer );
				cgi_error( E_FATAL, "Expected name in posted variable. (%s:%d)", __FILE__, __LINE__ );
			}

			p_file->filename = strndup( parser.filename.lexeme, parser.filename.length );

			if( !p_file->filename ) 
			{
				free( p_file->name );
				free( p_file );
				multipart_lexer_destroy( &lexer );
				cgi_error( E_MEMORY, "Unable to allocate memory. (%s:%d)", __FILE__, __LINE__ );
			}

			trim( p_file->filename, "\n\r\t " );
		
			if( parser.content_type.type != TOK_UNDEFINED )	
			{
				p_file->type = strndup( parser.content_type.lexeme, parser.content_type.length );
				if( !p_file->type ) 
				{
					free( p_file->name );
					free( p_file->filename );
					free( p_file );
					multipart_lexer_destroy( &lexer );
					cgi_error( E_MEMORY, "Unable to allocate memory. (%s:%d)", __FILE__, __LINE__ );
				}
				trim( p_file->type, "\n\r\t " );
			}
			else 
			{
				free( p_file->name );
				free( p_file->filename );
				free( p_file );
				multipart_lexer_destroy( &lexer );
				cgi_error( E_FATAL, "Expected content type in upload request. (%s:%d)", __FILE__, __LINE__ );
			}

			p_file->tmpfile = tmpfile( );
			if( !p_file->tmpfile )
			{
				free( p_file->name );
				free( p_file->filename );
				free( p_file->type );
				free( p_file );
				multipart_lexer_destroy( &lexer );
				cgi_error( E_FATAL, "Unable to create a temporary filename. (%s:%d)", __FILE__, __LINE__ );
			}

			if( parser.data.type != TOK_UNDEFINED )	
			{
				size_t bytes_written = 0;
				
				/* Write out the file */
				while( parser.data.length - bytes_written > 0 )
				{
					bytes_written += fwrite( (void *) (parser.data.lexeme + bytes_written), sizeof(char), parser.data.length - bytes_written, p_file->tmpfile );
				}

				/* set the file size */
				fseek( p_file->tmpfile, 0, SEEK_END );
				p_file->size = ftell( p_file->tmpfile );

				fclose( p_file->tmpfile );

				// TODO
				assert( FALSE );
				p_file->tmpfile = NULL;
			}
			else
			{
				free( p_file->name );
				free( p_file->filename );
				free( p_file->type );
				free( p_file );
				multipart_lexer_destroy( &lexer );
				cgi_error( E_FATAL, "Expected data in upload request. (%s:%d)", __FILE__, __LINE__ );
			}
	
			hash_table_insert( &cgi.files, p_file );
		}
		else /* regular posted variable */
		{ 
			char *key   = NULL;
			char *value = NULL;

			if( parser.name.type != TOK_UNDEFINED )
			{
				key  = strndup( parser.name.lexeme, parser.name.length );
				if( !key )
				{
					multipart_lexer_destroy( &lexer );
					cgi_error( E_MEMORY, "Unable to allocate memory. (%s:%d)", __FILE__, __LINE__ );
				}
			}
			else
			{
				multipart_lexer_destroy( &lexer );
				cgi_error( E_FATAL, "Expected key in posted variable. (%s:%d)", __FILE__, __LINE__ );
			}

			if( parser.data.type != TOK_UNDEFINED )
			{
				value = strndup( parser.data.lexeme, parser.data.length );
				if( !value )
				{
					free( key );
					multipart_lexer_destroy( &lexer );
					cgi_error( E_MEMORY, "Unable to allocate memory. (%s:%d)", __FILE__, __LINE__ );
				}
				trim( value, "\n\r\t " );
			}
			else
			{
				free( key );
				multipart_lexer_destroy( &lexer );
				cgi_error( E_FATAL, "Expected value in posted variable. (%s:%d)", __FILE__, __LINE__ );
			}

			assert( key );
			assert( value );
			
			hash_map_insert( p_map, key, value );
		}
	}  	

	multipart_lexer_destroy( &lexer );
}

void _cgi_process_cookies( char *cookies, hash_map_t *p_map )
{
	const char *delimiters = ";";
	char *token            = strtok( cookies, delimiters );

	while( token != NULL ) 
	{
		trim( token, "\n\r\t " );
		char *equal_sign_index  = strchr( token, '=' );

		/* Allocate memory for key and value strings */
		*equal_sign_index = '\0';
		char *key         = strdup( token );
		char *value       = strdup( equal_sign_index + 1 );

		url_decode( key );
		url_decode( value );

		hash_map_insert( p_map, key, value );

		token = strtok( NULL, delimiters );
	}	
}


void cgi_process_query_string( )
{
	char *query_string = strdup( cgi_query_string( ) );
	_cgi_process_query_string( query_string, &cgi.get_variables );	
	free( query_string );
}

void cgi_process_post( )
{
	const char *request_content_type = cgi_content_type( );	
	size_t request_content_length     = atol( cgi_content_length( ) );

	/* TODO: It is probably more efficient to avoid copying the 
     * post buffer. This is especially true for uploaded files.
	 */
	cgi_fill_post_buffer( stdin, request_content_length );

	/* Process the post string */
	if( strncasecmp( request_content_type, CGI_FORM_URL_ENCODED, CGI_FORM_URL_ENCODED_LENGTH ) == 0 ) 
	{
		_cgi_process_query_string( cgi.post_buffer, &cgi.post_variables );	
	}
	else if( strncasecmp( request_content_type, CGI_MULTIPART_FORM_DATA, CGI_MULTIPART_FORM_DATA_LENGTH ) == 0 ) 
	{
		_cgi_process_multipart_form_data( cgi.post_buffer, request_content_length, strchr(request_content_type, '=' ) + 1, &cgi.post_variables );	
	}
	else
	{
		cgi_error( E_FATAL, "I don't know how to process form data encoded as %s. (%s:%d)", request_content_type, __FILE__, __LINE__ );
	}
}

void cgi_process_cookies( )
{
	char *cookies = strdup( cgi_cookies( ) );
	_cgi_process_cookies( cookies, &cgi.cookies );	
	free( cookies );
}

void cgi_process_request( )
{
	const char *request_method_string = cgi_request_method( );
	request_method_type method         = UNDEFINED;

	cgi_log( E_INFO, _T("Processing request from %s (%s).\n"), cgi_remote_address( ), cgi_http_user_agent( ) );
	

	if( strcmp( request_method_string, _T("HEAD") ) == 0 )         { method = HEAD; }
	else if( strcmp( request_method_string, _T("GET") ) == 0 )     { method = GET; }
	else if( strcmp( request_method_string, _T("POST") ) == 0 )    { method = POST; cgi_process_post( ); }
	else if( strcmp( request_method_string, _T("PUT") ) == 0 )     { method = PUT; }
	else if( strcmp( request_method_string, _T("DELETE") ) == 0 )  { method = DELETE; }
	else if( strcmp( request_method_string, _T("TRACE") ) == 0 )   { method = TRACE; }
	else if( strcmp( request_method_string, _T("OPTIONS") ) == 0 ) { method = OPTIONS; }
	else if( strcmp( request_method_string, _T("CONNECT") ) == 0 ) { method = CONNECT; }
	else
	{
		#ifndef _DEBUG_CGI
		cgi_error( E_FATAL, _T("Unknown request method! (%s:%d)\n"), __FILE__, __LINE__ );
		#endif
	}


	cgi.request_method = method;

	cgi_process_query_string( );
	cgi_process_cookies( );

	if( cgi_is_enabled(CGI_OPT_ENABLE_SESSIONS, cgi.options) )
	{
		/*
 		 * Start a session if one has not been created
 		 * yet.
 		 */
		cgi_session_start( );	
	}

	cgi_path_info_create( NULL );

	/* Set up default content type */
	cgi_headerf( TRUE, _T("Content-Type: text/html; charset=UTF-8") ); 

	/* Set default headers */
	#ifdef NO_PROCESS_HEADERS
	time_t rawtime;
	struct tm * timeinfo;

	time( &rawtime );
	timeinfo = gmtime( &rawtime );
	char *date_string = asctime( timeinfo );
	rtrim(date_string);
	cgi_headerf( TRUE, _T("Server: %s"), cgi.application_name );
	cgi_headerf( TRUE, _T("Date: %s"), date_string ); 
	#endif
}


boolean cgi_headerf( boolean replace, char *header_fmt, ... )
{
	va_list arguments;
	char header_buffer[ 256 ];
	char *header_string = NULL;

	assert( header_fmt );

	if( cgi_is_enabled(CGI_STS_HEADERS_SENT, cgi.state) )
	{
		cgi_error( E_WARNING, _T("Cannot add header to response because the headers were already sent. (%s:%d)\n"), __FILE__, __LINE__ );
		return FALSE;
	}

	
	va_start( arguments, header_fmt );
	vsnprintf( header_buffer, sizeof(header_buffer), header_fmt, arguments );
	va_end( arguments );


	/* Ensure that the header is of the expected format (i.e Header-Key: Header-Value) */
	if( strchr( header_buffer, ':' ) == NULL )
	{
		cgi_error( E_WARNING, "A malformed header was detected. A ':' was expected somewhere in there. (%s:%d)\n", __FILE__, __LINE__ );
		return FALSE;
	}


	/* clean up the header string to remove any extra \r or \n chars in it */	
	trim( header_buffer, "\n\r\t " );

	assert( strlen(header_buffer) > 0 );


	header_string = strdup( header_buffer );
	if( !header_string )
	{
		cgi_error( E_MEMORY, "No memory available to allocate header buffer. (%s:%d)\n", __FILE__, __LINE__ );
		return FALSE;
	}

	if( replace )
	{
		#ifdef HEADERS_HASHED
		hash_table_remove( &cgi.response_headers, header_buffer );
		#else
		rbtree_remove( &cgi.response_headers, header_buffer );
		#endif
	}

	/* Previous header was not found so let's add it */	
	#ifdef HEADERS_HASHED
	return hash_table_insert( &cgi.response_headers, header_string );
	#else
	return rbtree_insert( &cgi.response_headers, header_string );
	#endif
}

void cgi_out( const char *format, ... )
{
	va_list ap;

	va_start( ap, format );
	buffer_vprintf( &cgi.output_buffer, format, ap );
	va_end( ap );
}

void cgi_send_headers( )
{
	/* Send headers if not already sent */
	if( cgi_is_disabled(CGI_STS_HEADERS_SENT, cgi.state) )
	{
		/* Write out the headers */
		#ifdef HEADERS_HASHED
		int i;
		for( i = 0; i < hash_table_table_size(&cgi.response_headers); i++ )
		{
			slist *p_list = &cgi.response_headers.table[ i ];
		
			if( slist_size( p_list ) > 0 )
			{
				slist_node *p_node = slist_head( p_list );

				while( p_node != NULL )
				{
					const char *header = (const char *) p_node->data;
					printf( "%s%s", header, CGI_CRLR );
					p_node = p_node->next;
				}
			}
		}
		#else
		rbtree_iterator_t itr;
		
		for( itr = rbtree_begin( &cgi.response_headers ); 
		     itr != rbtree_end( ); 
		     itr = rbtree_next(itr) )
		{
			const char *header = (const char *) itr->data;
			printf( "%s%s", header, CGI_CRLR );
		}
		#endif
	
		cgi_path_info_destroy( );
	
		cgi_set_bit( CGI_STS_HEADERS_SENT, cgi.state );
	}
}

boolean cgi_path_info_create( const char *path_info )
{
	if( !path_info )
	{
		path_info = cgi_path_info( );
	}
	
	char *path_info_str = strdup( path_info );

	const char *delims = _T("/");
	char *token        = strtok( path_info_str, delims );

	cgi.path_info.count = 0;

	while( token )
	{
		int count                    = cgi.path_info.count++;
		cgi.path_info.parts[ count ] = strdup( token );
	
		token = strtok( NULL, delims );
	}

	free( path_info_str );
	return TRUE;
}

void cgi_path_info_destroy( void )
{
	int i;

	for( i = 0; i < cgi.path_info.count; i++ )
	{
		char *part = cgi.path_info.parts[ i ];

		if( part )
		{
			free( part );
		}
	}

	cgi.path_info.count = 0;
}

void cgi_send_response( )
{
	#ifdef NO_PROCESS_HEADERS
	/* Write out the HTTP Status Line */
	printf( "%s %d %s%s", cgi_server_protocol( ), 200, http_status_reason(200), CGI_CRLR ); 
	#endif

	#ifdef HEADERS_HASHED
	const char *location_header = "Location:";
	void *found_header = NULL;

	if( hash_table_find( &cgi.response_headers, location_header, &found_header ) )
	{
		/* We are going to redirect so don't bother sending the 
 		 * output buffer.
 		 */
		buffer_clear( &cgi.output_buffer );
	}
	#else
	const char *location_header = "Location:";

	if( rbtree_search( &cgi.response_headers, location_header ) )
	{
		/* We are going to redirect so don't bother sending the 
 		 * output buffer.
 		 */
		buffer_clear( &cgi.output_buffer );
	}
	#endif
	
	/* Write out the headers */
	cgi_send_headers( );

	
	/* Write out blank line. This is required. */	
	printf( CGI_CRLR );

	if( cgi.request_method != HEAD )
	{
		buffer_flush( &cgi.output_buffer );
	}
}

void cgi_redirect( const char *url )
{
	assert( url );

	if( cgi_is_enabled( CGI_STS_HEADERS_SENT, cgi.state ) )
	{
		cgi_error( E_WARNING, _T("Cannot redirect because the headers were already sent. (%s:%d)\n"), __FILE__, __LINE__ );
		return;
	}
	
	cgi_headerf( TRUE, _T("Location: %s"), url );
}

void cgi_redirectf( const char *url_fmt, ... )
{
	va_list ap;
	char url_buffer[ 512 ];

	va_start( ap, url_fmt );
	vsnprintf( url_buffer, sizeof(url_buffer), url_fmt, ap );
	cgi_redirect( url_buffer );
	va_end( ap );
}

const char *cgi_get( const char *key )
{
	char *value       = NULL;	
	boolean key_found = hash_map_find( &cgi.get_variables, key, (void**) &value ); 

	if( key_found )
	{
		assert( value );
		return value;
	}

	return NULL;
}

const char *cgi_post( const char *key )
{
	char *value       = NULL;	
	boolean key_found = hash_map_find( &cgi.post_variables, key, (void**) &value ); 

	if( key_found )
	{
		assert( value );
		return value;
	}

	return NULL;
}

const cgi_file* cgi_files( const char *key )
{
	char *value = NULL;	

	cgi_file file;
	file.name = (char *) key;	

	boolean key_found = hash_table_find( &cgi.files, &file, (void**) &value ); 

	if( key_found )
	{
		assert( value );
		return (cgi_file*) value;
	}

	return NULL;
}

void cgi_include( const char *filename )
{
	char buffer[ 1024 ];
	FILE *file = fopen( (char *) filename, "rb" );

	if( !file )
	{
		cgi_error( E_FATAL, _T("Unable to open %s. %s (%s:%d)\n"), filename, strerror(errno), __FILE__, __LINE__ );
	}

	while( !feof(file) )
	{
		if( fgets( buffer, sizeof(buffer), file ) )
		{
			cgi_out( _T("%s"), buffer );
		}
	}

	fclose( file );
}

static char url_buffer[ 512 ];

const char *cgi_urlf( const char *protocol, const char *hostname, const char *relative_url_fmt, ... )
{
	va_list arguments;
	char relative_url_buffer[ 256 ];
	char *start = relative_url_buffer;


	va_start( arguments, relative_url_fmt );
	vsnprintf( relative_url_buffer, sizeof(relative_url_buffer),  
		relative_url_fmt,
		arguments
	);
	va_end( arguments );

	if( *start == '/' )
	{
		/* skip over '/' */
		start++;
	}

	snprintf( url_buffer, sizeof(url_buffer), _T("%s://%s/%s"), 
		protocol ? protocol : _T("http"), 
		hostname ? hostname : cgi.domain, 
		start
	);

	return url_buffer;
}

boolean key_value_destroy( void *key, void *value )
{
	free( key );
	free( value );
	return TRUE;
}

int key_compare( const void *left, const void *right )
{
	return strcmp( (const char*) left, (const char*) right);
}

#if defined(HEADERS_HASHED)
size_t header_hash( const void *header )
{
	size_t hash       = 0;
	size_t count      = strchr( header, ':' ) - ((char*)header);
	char *header_type = substr( (char *) header, 0, count ); /* TODO: Optimize out the memory alloc */

	hash = string_hash( header_type );
	free( header_type );

	return hash;
}
#endif

boolean header_destroy( void *header )
{
	free( header );
	return TRUE;
}

int header_compare( const void *left, const void *right )
{
	const char *left_string    = (char *) left;
	const char *right_string   = (char *) right;
	size_t num_of_chars_to_cmp  = strchr( left_string, ':' ) - left_string;

	assert( num_of_chars_to_cmp <= 100 );
	assert( left_string );
	assert( right_string );

	return strncasecmp( left_string, right_string, num_of_chars_to_cmp );
}

size_t cgi_file_hash( const void *file )
{
	cgi_file *p_file  = (cgi_file *) file;
	assert( p_file );
	return string_hash( p_file->name );
}

boolean cgi_file_destroy( void *file )
{
	cgi_file *p_file  = (cgi_file *) file;
	assert( p_file );
	free( p_file->name );
	free( p_file->filename );
	free( p_file->type );
	return TRUE;
}

int cgi_file_compare( const void *left, const void *right )
{
	cgi_file *p_left  = (cgi_file *) left;
	cgi_file *p_right = (cgi_file *) right;
	assert( p_left );
	assert( p_right );
	return strcasecmp( p_left->name, p_right->name );
}

/*
 * CGI Environment Variables
 */
const char *cgi_server_software( )
{
	const char *server_software = getenv( "SERVER_SOFTWARE" );
	return server_software ? server_software : CGI_EMPTY_STRING;
}

const char *cgi_server_name( )
{
	const char *server_name = getenv( "SERVER_NAME" );
	return server_name ? server_name : CGI_EMPTY_STRING;
}

const char *cgi_server_protocol( )
{
	const char *server_protocol = getenv( "SERVER_PROTOCOL" );
	return server_protocol ? server_protocol : CGI_DEFAULT_PROTOCOL;
}

const char *cgi_server_port( )
{
	const char *server_port = getenv( "SERVER_PORT" );
	return server_port ? server_port : CGI_EMPTY_STRING;
}

const char *cgi_server_admin( )
{
	const char *server_admin = getenv( "SERVER_ADMIN" );
	return server_admin ? server_admin : CGI_EMPTY_STRING;
}

const char *cgi_is_https( )
{
	const char *gateway_interface = getenv( "HTTPS" );
	return gateway_interface ? gateway_interface : CGI_EMPTY_STRING;
}

const char *cgi_request_method( )
{
	const char *request_method = getenv( "REQUEST_METHOD" );
	return request_method ? request_method : CGI_EMPTY_STRING;
}

const char *cgi_script_name( )
{
	const char *script_name = getenv( "SCRIPT_NAME" );
	return script_name ? script_name : CGI_EMPTY_STRING;
}

const char *cgi_script_filename( )
{
	const char *script_filename = getenv( "SCRIPT_FILENAME" );
	return script_filename ? script_filename : CGI_EMPTY_STRING;
}

const char *cgi_path( )
{
	const char *path = getenv( "PATH" );
	return path ? path : CGI_EMPTY_STRING;
}

const char *cgi_path_info( )
{
	const char *path_info = getenv( "PATH_INFO" );
	return path_info ? path_info : CGI_EMPTY_STRING;
}

const char *cgi_path_translated( )
{
	const char *path_translated = getenv( "PATH_TRANSLATED" );
	return path_translated ? path_translated : CGI_EMPTY_STRING;
}

const char *cgi_query_string( )
{
	const char *query_string = getenv( "QUERY_STRING" );
	return query_string ? query_string : CGI_EMPTY_STRING;
}

const char *cgi_request_uri( )
{
	const char *r = getenv( "REQUEST_URI" );
	return r ? r : CGI_EMPTY_STRING;
}

const char *cgi_remote_host( )
{
	const char *remote_host = getenv( "REMOTE_HOST" );
	return remote_host ? remote_host : CGI_EMPTY_STRING;
}

const char *cgi_remote_address( )
{
	const char *remote_address = getenv( "REMOTE_ADDR" );
	return remote_address ? remote_address : CGI_EMPTY_STRING;
}

const char *cgi_remote_user( )
{
	const char *remote_user = getenv( "REMOTE_USER" );
	return remote_user ? remote_user : CGI_EMPTY_STRING;
}

const char *cgi_remote_port( )
{
	const char *remote_port = getenv( "REMOTE_PORT" );
	return remote_port ? remote_port : CGI_EMPTY_STRING;
}

const char *cgi_remote_ident( )
{
	const char *remote_ident = getenv( "REMOTE_IDENT" );
	return remote_ident ? remote_ident : CGI_EMPTY_STRING;
}

const char *cgi_auth_type( )
{
	const char *auth_type = getenv( "AUTH_TYPE" );
	return auth_type ? auth_type : CGI_EMPTY_STRING;
}

const char *cgi_content_type( )
{
	const char *content_type = getenv( "CONTENT_TYPE" );
	return content_type ? content_type : CGI_EMPTY_STRING;
}

const char *cgi_content_length( )
{
	const char *content_length = getenv( "CONTENT_LENGTH" );
	return content_length ? content_length : CGI_ZERO_STRING;
}

const char *cgi_http_host( )
{
	const char *host = getenv( "HTTP_HOST" );
	return host ? host : CGI_EMPTY_STRING;
}

const char *cgi_http_accept( )
{
	const char *accept = getenv( "HTTP_ACCEPT" );
	return accept ? accept : CGI_EMPTY_STRING;
}

const char *cgi_http_referer( )
{
	const char *accept = getenv( "HTTP_REFERER" );
	return accept ? accept : CGI_EMPTY_STRING;
}

const char *cgi_http_user_agent( )
{
	const char *user_agent = getenv( "HTTP_USER_AGENT" );
	return user_agent ? user_agent : CGI_EMPTY_STRING;
}

const char *cgi_document_root( )
{
	const char *user_agent = getenv( "DOCUMENT_ROOT" );
	return user_agent ? user_agent : CGI_EMPTY_STRING;
}

const char *cgi_cookies( )
{
	const char *c = getenv( "HTTP_COOKIE" );
	return c ? c : CGI_EMPTY_STRING;
}
