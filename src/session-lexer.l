/* flex -o session-lexer.c --prefix=session_  --header-file=session-lexer.h session-lexer.l */
%{
#include <stdio.h>
#include <string.h>
#include "session-parser.h"

#ifndef SESSION_LEXER_TEST
#define YY_NO_INPUT   
#define ECHO
#endif

%}


%% /* ------------------------------------------- */
(\r\n\r\n)		          { return SESS_TOK_CRLFCRLF; }
[0-9a-zA-Z_-]+            { session_lval = strdup(session_text); return SESS_TOK_KEY; }
=[^(\r\n\r\n)]*           { session_lval = strdup(session_text + 1); return SESS_TOK_VALUE; }
\n                        ;
%% /* ------------------------------------------- */

int session_wrap( void ) { return 1; }

#ifdef SESSION_LEXER_TEST
int main( int argc, char* argv[] )
{
	session_lex();
	return 0;
}
#endif
