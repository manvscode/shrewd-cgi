/* bison -o session-parser.c -d --name-prefix=session_ session-parser.y */
/* gcc -g -ggdb -O0 -DSESSION_PARSER_TEST  session-lexer.c session-parser.c -o session-test */
%{
#include <stdio.h>
#include <string.h>
#include "session-lexer.h"
#include "cgi.h"

//#define SESSION_PARSER_TEST

#define YYSTYPE_IS_DECLARED
#define YYSTYPE       char*

void session_error( const char *error );
extern boolean session_parser_set( const char *key, const char *value );
%}

%token SESS_TOK_KEY SESS_TOK_VALUE SESS_TOK_CRLFCRLF
/*
%union {
	char* text;
}
%type<text> SESS_TOK_KEY SESS_TOK_VALUE SESS_TOK_CRLFCRLF
*/
%start key_val_list

%%

key_val_list:
	key_val                            
	| key_val_list key_val
	;


key_val:
	SESS_TOK_KEY SESS_TOK_VALUE SESS_TOK_CRLFCRLF			{ 
																#ifdef SESSION_PARSER_TEST
																printf( "  ===>  %s --> [%s]\n", $1, $2 );
																free( $1 );
																free( $2 );
																#else
																if( !session_parser_set( $1, $2) )
																{
																	free( $1 );
																	free( $2 );
																	session_error( "Failed to process session key-value." );
																}
																#endif
															}
															;


%%

void session_error( const char *error )
{
	#ifdef SESSION_PARSER_TEST
	fprintf( stderr, "Failed to process session record. \n" );
	#else
	cgi_log( E_FATAL, error );
	//cgi_error( E_FATAL, "Failed to process session record." );
	#endif
}

#ifdef SESSION_PARSER_TEST
int main( int argc, char *argv[] )
{
	if( argc > 1 )
	{
		session_in = fopen( argv[1],"r" );
		session_parse( );
	}

	return 0;
}
#endif
