/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <mhash.h>
#include "cgi.h"
#include "base64.h"

boolean cgi_hash( hashid type, void *result, const void *plaintext, size_t size )
{
	MHASH td;
	td = mhash_init( (hashid) type );

	if( td == MHASH_FAILED )
	{
		cgi_error( E_WARNING, "%s initialization failed. (%s:%ld)\n", mhash_get_hash_name_static((hashid) type),  __FILE__, __LINE__ );
		return FALSE;
	}

	mhash( td, plaintext, size );
	mhash_deinit( td, result );
	return TRUE;
}


char *cgi_bin2hex( void *hash, size_t size )
{
	char *result;
	size_t i;
	byte *hash_bytes = (byte *) hash;
	size_t result_size = 2 * size + 1;
	static char hex_chars[] = {
		'0', '1', '2', '3', '4', '5', '6', '7',
		'8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
	};

	result = (char *) malloc( result_size );

	if( !result )
	{
		cgi_error( E_MEMORY, "Ran out of memory. (%s:%ld)", __FILE__, __LINE__ );
		return NULL;
	}


	for( i = 0; i < size; i++ )
	{
		result[ 2 * i     ] = hex_chars[ hash_bytes[ i ] >> 4 ];
		result[ 2 * i + 1 ] = hex_chars[ hash_bytes[ i ] & 0x0F ];	
	}

	result[ result_size - 1 ] = '\0';

	return result;
}

char *cgi_md5( const void *plaintext, size_t size )
{
	byte hash[ CGI_MD5_SIZE ];
	assert( CGI_MD5_SIZE == mhash_get_block_size(MHASH_MD5) );

	if( !cgi_hash( MHASH_MD5, hash, plaintext, size ) ) /* hashing failed */
	{
		cgi_error( E_WARNING, "MD5 hashing failed. (%s:%ld)\n", __FILE__, __LINE__ );
		return NULL;
	}


	return cgi_bin2hex( hash, CGI_MD5_SIZE );
}

char *cgi_sha1( const void *plaintext, size_t size )
{
	byte hash[ CGI_SHA1_SIZE ];
	assert( CGI_SHA1_SIZE == mhash_get_block_size(MHASH_SHA1) );

	if( !cgi_hash( MHASH_SHA1, hash, plaintext, size ) ) /* hashing failed */
	{
		cgi_error( E_WARNING, "SHA1 hashing failed. (%s:%ld)\n", __FILE__, __LINE__ );
		return NULL;
	}

	return cgi_bin2hex( hash, CGI_SHA1_SIZE );
}

char *cgi_sha256( const void *plaintext, size_t size )
{
	byte hash[ CGI_SHA256_SIZE ];
	assert( CGI_SHA256_SIZE == mhash_get_block_size(MHASH_SHA256) );

	if( !cgi_hash( MHASH_SHA256, hash, plaintext, size ) ) /* hashing failed */
	{
		cgi_error( E_WARNING, "SHA256 hashing failed. (%s:%ld)\n", __FILE__, __LINE__ );
		return NULL;
	}

	return cgi_bin2hex( hash, CGI_SHA256_SIZE );
}

char *cgi_sha512( const void *plaintext, size_t size )
{
	byte hash[ CGI_SHA512_SIZE ];
	assert( CGI_SHA512_SIZE == mhash_get_block_size(MHASH_SHA512) );

	if( !cgi_hash( MHASH_SHA512, hash, plaintext, size ) ) /* hashing failed */
	{
		cgi_error( E_WARNING, "SHA512 hashing failed. (%s:%ld)\n", __FILE__, __LINE__ );
		return NULL;
	}

	return cgi_bin2hex( hash, CGI_SHA512_SIZE );
}


