/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include "multipart.h"
#include "cgi.h"

const char* multipart_token_lookup_table[] = {
	NULL,                         /* TOK_END */
	NULL,                         /* TOK_DATA */
	NULL,                         /* TOK_BOUNDARY */
	NULL,                         /* TOK_BOUNDARY_END */
	"Content-Disposition:",       /* TOK_CONTENT_DISPOSITION_HEADER */
	"Content-Type:",              /* TOK_CONTENT_TYPE_HEADER */
	"Content-Transfer-Encoding:", /* TOK_CONTENT_TRANSFER_ENCODING_HEADER */
	"form-data",                  /* TOK_FORM_DATA */
	"name",                       /* TOK_NAME */
	"filename",                   /* TOK_FILENAME */
	"=",                          /* TOK_EQUAL */
	"\"",                         /* TOK_QUOTE */
	";",                          /* TOK_SEMICOLON */
	"\r\n",                       /* TOK_CRLF */
	NULL
};

const char* multipart_token_string_table[] = {
	"TOK_END",
	"TOK_DATA",
	"TOK_BOUNDARY",
	"TOK_BOUNDARY_END",
	"TOK_CONTENT_DISPOSITION_HEADER",
	"TOK_CONTENT_TYPE_HEADER",
	"TOK_CONTENT_TRANSFER_ENCODING_HEADER",
	"TOK_FORM_DATA",
	"TOK_NAME",
	"TOK_FILENAME",
	"TOK_EQUAL",
	"TOK_QUOTE",
	"TOK_SEMICOLON",
	"TOK_CRLF",
	NULL
};

#define HIT_TEST(string, other_string, other_string_length) \
	(strncmp( string, other_string, other_string_length ) == 0)
#define HIT_TEST_I(string, other_string, other_string_length) \
	(strncasecmp( string, other_string, other_string_length ) == 0)


/*
 *  Multipart/form-data Lexer Functions
 */	
void multipart_lexer_create( multipart_lexer *p_lexer, const char *p_buffer, size_t buffer_size, const char *boundary ) 
{
	size_t boundary_length = strlen( boundary );
	assert( p_lexer );

	p_lexer->buffer          = p_buffer;
	p_lexer->buffer_size     = buffer_size;	
	p_lexer->position        = 0;	

	/* Create boundary string */
	{
		p_lexer->boundary_length = 2 + boundary_length; // "--" + boundary
		p_lexer->boundary        = (char *) malloc( p_lexer->boundary_length + 1 );	
		strncpy( p_lexer->boundary, "--", 2 );
		strncpy( p_lexer->boundary + 2, boundary, boundary_length );
		p_lexer->boundary[ p_lexer->boundary_length ] = '\0';
	}

	/* Create the end boundary string */
	{
		p_lexer->end_boundary_length = 2 + boundary_length + 2; // "--" + boundary + "--"
		p_lexer->end_boundary        = (char *) malloc( p_lexer->end_boundary_length + 1 );	
		strncpy( p_lexer->end_boundary, "--", 2 );
		strncpy( p_lexer->end_boundary + 2, boundary, boundary_length );
		strncpy( p_lexer->end_boundary + 2 + boundary_length, "--", 2 );
		p_lexer->end_boundary[ p_lexer->end_boundary_length ] = '\0';
	}

	memset( &p_lexer->token, 0, sizeof(multipart_token) );
}

void multipart_lexer_destroy( multipart_lexer *p_lexer ) 
{
	free( p_lexer->boundary );
	free( p_lexer->end_boundary );
}

const char* multipart_lexer_lookup_token( multipart_token_type token_type ) 
{
	assert( (token_type) >= 0 ); 
	assert( (token_type) < sizeof(multipart_token_lookup_table) ); 
	return multipart_token_lookup_table[ token_type ];
}

const char* multipart_lexer_token_string( multipart_token_type token_type ) 
{
	assert( (token_type) >= 0 ); 
	assert( (token_type) < sizeof(multipart_token_string_table) ); 
	return multipart_token_string_table[ token_type ];
}

multipart_token_type multipart_lexer_next_token( multipart_lexer *p_lexer ) 
{
	int i;

	assert( p_lexer );

	p_lexer->token.type   = TOK_END;
	p_lexer->token.lexeme = NULL;
	p_lexer->token.length = 0;

	if( isblank( p_lexer->buffer[ p_lexer->position ] ) ) 
	{
		p_lexer->position++;
	}

	/* Test for the ending boundary token... */
	if( HIT_TEST( &p_lexer->buffer[ p_lexer->position ], p_lexer->end_boundary, p_lexer->end_boundary_length ) ) 
	{
		p_lexer->token.type   = TOK_BOUNDARY_END;
		p_lexer->token.lexeme = &p_lexer->buffer[ p_lexer->position ];
		p_lexer->token.length = p_lexer->end_boundary_length;

		p_lexer->position += p_lexer->end_boundary_length;
	}
	/* Test for the boundary token... */
	else if( HIT_TEST( &p_lexer->buffer[ p_lexer->position ], p_lexer->boundary, p_lexer->boundary_length ) ) 
	{
		p_lexer->token.type   = TOK_BOUNDARY;
		p_lexer->token.lexeme = &p_lexer->buffer[ p_lexer->position ];
		p_lexer->token.length = p_lexer->boundary_length;

		p_lexer->position += p_lexer->boundary_length;
	}
	else 
	{
		/* Test for the other tokens... */
		for( i = TOK_CONTENT_DISPOSITION_HEADER; i < MULTIPART_TOKEN_COUNT; i++ ) 
		{
			const char* token_string   = multipart_lexer_lookup_token( i );
			size_t token_string_length = strlen( token_string );

			if( HIT_TEST_I( &p_lexer->buffer[ p_lexer->position ], token_string, token_string_length ) ) 
			{
				p_lexer->token.type   = (multipart_token_type) i;
				p_lexer->token.lexeme = &p_lexer->buffer[ p_lexer->position ];
				p_lexer->token.length = token_string_length;
				
				p_lexer->position += token_string_length;
				
				break;
			}
		}
	}

	if( p_lexer->token.type == TOK_END ) 
	{
		boolean token_test = FALSE;
	
		while( p_lexer->position < p_lexer->buffer_size && !token_test ) 
		{
			token_test = HIT_TEST( &p_lexer->buffer[ p_lexer->position ], p_lexer->end_boundary, p_lexer->end_boundary_length )
			             || HIT_TEST( &p_lexer->buffer[ p_lexer->position ], p_lexer->boundary, p_lexer->boundary_length );
			             //|| HIT_TEST( &p_lexer->buffer[ p_lexer->position ], multipart_lexer_lookup_token(TOK_CRLF), 2 );

			if( !token_test && !p_lexer->reading_file ) 
			{
				/* Test for the other tokens... */
				for( i = TOK_CONTENT_DISPOSITION_HEADER; i < MULTIPART_TOKEN_COUNT; i++ ) 
				{
					const char* token_string   = multipart_lexer_lookup_token( i );
					size_t token_string_length = strlen( token_string );

					if( HIT_TEST_I( &p_lexer->buffer[ p_lexer->position ], token_string, token_string_length ) ) 
					{
						token_test = TRUE;	
						break;
					}
				}
			}

			if( !token_test ) 
			{
				if( p_lexer->token.type != TOK_DATA ) 
				{
					/* Ok We found a data token */
					p_lexer->token.type   = TOK_DATA;
					p_lexer->token.lexeme = &p_lexer->buffer[ p_lexer->position ];
				}
				p_lexer->token.length++;
				p_lexer->position++;
			}
		}
	}

	#ifdef _DEBUG_MULTIPART
	char *lexeme = strndup( p_lexer->token.lexeme, p_lexer->token.length );
	if( lexeme != NULL ) 
	{
		lexeme[ p_lexer->token.length ] = '\0';
		cgi_out( "%60s (%s) (is_file = %d) (reading_file = %d)\n", lexeme, multipart_lexer_token_string(p_lexer->token.type), p_lexer->is_file, p_lexer->reading_file ); 
		free( lexeme );
	}
	#endif

	return p_lexer->token.type;
}


/*
 *  Multipart/form-data Parser Functions
 */	
boolean multipart_parser_eat( multipart_lexer *p_lexer, multipart_token_type token ) 
{
	if( p_lexer->token.type == token ) 
	{
		multipart_lexer_advance( p_lexer );
		return TRUE;
	}

	return FALSE;
}

void multipart_parser_eate( multipart_lexer *p_lexer, multipart_token_type token ) 
{
	if( !multipart_parser_eat( p_lexer, token ) ) 
	{
		cgi_error( E_FATAL, "Expected a %s but got a %s. (%s:%d)", 
		           multipart_lexer_token_string(token), 
                   multipart_lexer_token_string(p_lexer->token.type),
		           __FILE__,
		           __LINE__
		);
	}
}

void multipart_parser_handle_part( multipart_parser *p_parser, multipart_lexer *p_lexer ) 
{
	assert( p_parser );
	assert( p_lexer );
	
	memset( p_parser, 0, sizeof(multipart_parser) );

	switch( p_lexer->token.type ) 
	{
		case TOK_BOUNDARY:
			p_lexer->is_file      = FALSE;
			p_lexer->reading_file = FALSE;

			multipart_parser_eate( p_lexer, TOK_BOUNDARY );
			multipart_parser_eate( p_lexer, TOK_CRLF );
			while( p_lexer->token.type != TOK_CRLF ) 
			{
				multipart_parser_handle_header( p_parser, p_lexer );
			}
			if( p_lexer->is_file ) p_lexer->reading_file = TRUE;
			multipart_parser_eate( p_lexer, TOK_CRLF );
			
			/* Copy posted variable data and advance */
			if( p_lexer->token.type == TOK_DATA ) 
			{
				memcpy( &p_parser->data, &p_lexer->token, sizeof(multipart_token) ); 
				multipart_lexer_advance( p_lexer );
			}
			else /* We need to store an empty string for a blank posted variable */
			{ 
				multipart_token token;
				token.type   = TOK_DATA;
				token.lexeme = "";
				token.length = 0;
				memcpy( &p_parser->data, &token, sizeof(multipart_token) ); 
			}
		
			if( !p_lexer->reading_file )	multipart_parser_eate( p_lexer, TOK_CRLF );
			p_lexer->is_file      = FALSE;
			p_lexer->reading_file = FALSE;

			break;
		case TOK_BOUNDARY_END:
			multipart_parser_eate( p_lexer, TOK_BOUNDARY_END );
			multipart_parser_eate( p_lexer, TOK_CRLF );
			break;
		default:
			cgi_error( E_FATAL, "Expected %s, or %s but received %s %s. (%s:%d)", 
			           multipart_lexer_token_string(TOK_BOUNDARY), 
			           multipart_lexer_token_string(TOK_BOUNDARY_END), 
			           multipart_lexer_token_string(p_lexer->token.type), 
						p_lexer->token.lexeme,
			           __FILE__, __LINE__ );
			break;
	}
}

void multipart_parser_handle_header( multipart_parser *p_parser, multipart_lexer *p_lexer ) 
{
	assert( p_parser );
	assert( p_lexer );

	switch( p_lexer->token.type ) 
	{
		case TOK_CONTENT_DISPOSITION_HEADER:
			multipart_parser_eate( p_lexer, TOK_CONTENT_DISPOSITION_HEADER );
			multipart_parser_eate( p_lexer, TOK_FORM_DATA );
			multipart_parser_eate( p_lexer, TOK_SEMICOLON );
			multipart_parser_eate( p_lexer, TOK_NAME );
			multipart_parser_eate( p_lexer, TOK_EQUAL );
			multipart_parser_eate( p_lexer, TOK_QUOTE );

			/* Copy posted variable name and advance */
			if( p_lexer->token.type == TOK_DATA ) 
			{
				memcpy( &p_parser->name, &p_lexer->token, sizeof(multipart_token) ); 
				multipart_lexer_advance( p_lexer );
			}

			multipart_parser_eate( p_lexer, TOK_QUOTE );

			p_lexer->is_file = FALSE;
			while( p_lexer->token.type != TOK_CRLF ) 
			{
				multipart_parser_eate( p_lexer, TOK_SEMICOLON );
				multipart_parser_eate( p_lexer, TOK_FILENAME );
				multipart_parser_eate( p_lexer, TOK_EQUAL );
				multipart_parser_eate( p_lexer, TOK_QUOTE );

				/* Copy posted variable name and advance */
				if( p_lexer->token.type == TOK_DATA ) 
				{
					memcpy( &p_parser->filename, &p_lexer->token, sizeof(multipart_token) ); 
					multipart_lexer_advance( p_lexer );
				}

				multipart_parser_eate( p_lexer, TOK_QUOTE );
				p_lexer->is_file = TRUE;
			}
			break;
		case TOK_CONTENT_TYPE_HEADER:
			multipart_parser_eate( p_lexer, TOK_CONTENT_TYPE_HEADER );
			/* Copy posted variable content-type and advance */
			if( p_lexer->token.type == TOK_DATA ) 
			{
				memcpy( &p_parser->content_type, &p_lexer->token, sizeof(multipart_token) ); 
				multipart_lexer_advance( p_lexer );
			}
			break;
		case TOK_CONTENT_TRANSFER_ENCODING_HEADER:
			multipart_parser_eate( p_lexer, TOK_CONTENT_TRANSFER_ENCODING_HEADER );
			assert( FALSE ); /* NOT IMPLEMENTED YET */
			cgi_error( E_FATAL, "Cannot handle posts with special encodings (%s:%d).", __FILE__, __LINE__ );
			break;
		default:
			cgi_error( E_FATAL, "Expected %s, %s, or %s, but received %s. (%s:%d)", 
			           multipart_lexer_token_string(TOK_CONTENT_DISPOSITION_HEADER),  
			           multipart_lexer_token_string(TOK_CONTENT_TYPE_HEADER),  
			           multipart_lexer_token_string(TOK_CONTENT_TRANSFER_ENCODING_HEADER),  
			           multipart_lexer_token_string(p_lexer->token.type),
			           __FILE__, __LINE__ );
			break;
	}

	if( p_lexer->token.type == TOK_CRLF ) 
	{
		multipart_lexer_advance( p_lexer );
	}
}

