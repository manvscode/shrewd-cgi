#ifndef _URL_H_
#define _URL_H_

#include <libcollections/types.h>

/* URL-unescape the string S.

   This is done by transforming the sequences "%HH" to the character
   represented by the hexadecimal digits HH.  If % is not followed by
   two hexadecimal digits, it is inserted literally.

   The transformation is done in place.  If you need the original
   string intact, make a copy before calling this function.  */
void url_unescape (char *s);

void url_decode( char *s );

/* The core of url_escape_* functions.  Escapes the characters that
   match the provided mask in urlchr_table.

   If ALLOW_PASSTHROUGH is TRUE, a string with no unsafe chars will be
   returned unchanged.  If ALLOW_PASSTHROUGH is FALSE, a freshly
   allocated string will be returned in all cases.  */
char *url_escape_1 (const char *s, unsigned char mask, boolean allow_passthrough);

/* URL-escape the unsafe characters (see urlchr_table) in a given
   string, returning a freshly allocated string.  */
char *url_escape (const char *s);

/* URL-escape the unsafe characters (see urlchr_table) in a given
   string.  If no characters are unsafe, S is returned.  */
char *url_escape_allow_passthrough (const char *s);

#endif /* _URL_H_ */
