/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _BUFFER_H_
#define _BUFFER_H_

#include <stddef.h>
#include <libcollections/types.h>


typedef struct struct_buffer {
	char*  array;
	size_t array_size;
	size_t size;
} buffer;

boolean buffer_create     ( buffer *p_buffer, size_t buffer_size );
void    buffer_destroy    ( buffer *p_buffer );
boolean buffer_printf     ( buffer *p_buffer, const char *format, ... );
boolean buffer_vprintf    ( buffer *p_buffer, const char *format, va_list ap );
void    buffer_flush      ( buffer *p_buffer );
void    buffer_clear      ( buffer *p_buffer );

#define buffer_array(p_buffer)               ((p_buffer)->array)
#define buffer_array_size( p_buffer )        ((p_buffer)->array_size)
#define buffer_size( p_buffer )              ((p_buffer)->size)
#define buffer_element_at( p_buffer, index ) (buffer_array(p_buffer) + buffer_element_size(p_buffer) * (index))

#endif /* _BUFFER_H_ */
