/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifdef FCGI_BUILD
#include <fcgi_stdio.h> /* fcgi library; put it first*/
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include "buffer.h"
#include "cgi.h"

boolean buffer_create( buffer *p_buffer, size_t buffer_size ) 
{
	assert( p_buffer );

	p_buffer->size       = 0L;
	p_buffer->array_size = buffer_size;
	p_buffer->array      = malloc( sizeof(char) * buffer_array_size(p_buffer) );

	#ifdef _DEBUG_BUFFER
	memset( p_buffer->array, 0, sizeof(char) * buffer_array_size(p_buffer) );
	#endif

	assert( p_buffer->array );

	setvbuf( stdout, p_buffer->array, _IONBF,  sizeof(char) * buffer_array_size(p_buffer) );

	return p_buffer->array != NULL;
}

void buffer_destroy( buffer *p_buffer )
{
	assert( p_buffer );

	free( p_buffer->array );

	#ifdef _DEBUG_BUFFER
	p_buffer->array       = NULL;
	p_buffer->array_size  = 0L;
	p_buffer->size        = 0L;
	#endif
}

boolean buffer_printf( buffer *p_buffer, const char *format, ... ) 
{
	boolean result = TRUE;
	va_list ap;

	va_start( ap, format );
	result = buffer_vprintf( p_buffer, format, ap );
	va_end( ap );

	return result;	
}

boolean buffer_vprintf( buffer *p_buffer, const char *format, va_list ap ) 
{
	int ret;

	if( buffer_size(p_buffer) >= buffer_array_size(p_buffer) ) 
	{
		cgi_error( E_MEMORY, "The output buffer is full and no more data can be written. (%s:%d)\n", __FILE__, __LINE__ );
	}

	ret = vsnprintf( buffer_array(p_buffer) + buffer_size(p_buffer), buffer_array_size(p_buffer), format, ap );
	
	if( ret + buffer_array_size(p_buffer) < buffer_array_size(p_buffer) ) 
	{
		cgi_error( E_MEMORY, "Data was truncated when written to the output buffer. %d/%d bytes not written!  (%s:%ld)\n", ret, buffer_array_size(p_buffer), __FILE__, __LINE__ );
	}

	p_buffer->size += ret;

	return ret + buffer_array_size(p_buffer) < buffer_array_size(p_buffer);
}

void buffer_flush( buffer *p_buffer ) 
{
	fwrite( buffer_array(p_buffer), sizeof(char), buffer_size(p_buffer), stdout );
}

void buffer_clear( buffer *p_buffer ) 
{
	assert( p_buffer );
	#ifdef _DEBUG_BUFFER
	memset( buffer_array(p_buffer), 0, sizeof(char) * buffer_array_size(p_buffer) );
	#endif
	p_buffer->size = 0;
}

