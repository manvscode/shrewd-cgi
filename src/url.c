#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include "url.h"

/* Convert an ASCII hex digit to the corresponding number between 0
   and 15.  H should be a hexadecimal digit that satisfies isxdigit;
   otherwise, the result is undefined.  */
#define XDIGIT_TO_NUM(h) ((h) < 'A' ? (h) - '0' : toupper (h) - 'A' + 10)
#define X2DIGITS_TO_NUM(h1, h2) ((XDIGIT_TO_NUM (h1) << 4) + XDIGIT_TO_NUM (h2))

/* The reverse of the above: convert a number in the [0, 16) range to
   the ASCII representation of the corresponding hexadecimal digit.
   `+ 0' is there so you can't accidentally use it as an lvalue.  */
#define XNUM_TO_DIGIT(x) ("0123456789ABCDEF"[x] + 0)
#define XNUM_TO_digit(x) ("0123456789abcdef"[x] + 0)

/* Support for escaping and unescaping of URL strings.  */

/* Table of "reserved" and "unsafe" characters.  Those terms are
   rfc1738-speak, as such largely obsoleted by rfc2396 and later
   specs, but the general idea remains.

   A reserved character is the one that you can't decode without
   changing the meaning of the URL.  For example, you can't decode
   "/foo/%2f/bar" into "/foo///bar" because the number and contents of
   path components is different.  Non-reserved characters can be
   changed, so "/foo/%78/bar" is safe to change to "/foo/x/bar".  The
   unsafe characters are loosely based on rfc1738, plus "$" and ",",
   as recommended by rfc2396, and minus "~", which is very frequently
   used (and sometimes unrecognized as %7E by broken servers).

   An unsafe character is the one that should be encoded when URLs are
   placed in foreign environments.  E.g. space and newline are unsafe
   in HTTP contexts because HTTP uses them as separator and line
   terminator, so they must be encoded to %20 and %0A respectively.
   "*" is unsafe in shell context, etc.

   We determine whether a character is unsafe through static table
   lookup.  This code assumes ASCII character set and 8-bit chars.  */

enum {
	/* rfc1738 reserved chars + "$" and ",".  */
	urlchr_reserved = 1,

	/* rfc1738 unsafe chars, plus non-printables.  */
	urlchr_unsafe   = 2
};

#define urlchr_test(c, mask) (urlchr_table[(unsigned char)(c)] & (mask))
#define URL_RESERVED_CHAR(c) urlchr_test(c, urlchr_reserved)
#define URL_UNSAFE_CHAR(c) urlchr_test(c, urlchr_unsafe)

/* Shorthands for the table: */
#define R  urlchr_reserved
#define U  urlchr_unsafe
#define RU R|U

static const unsigned char urlchr_table[256] =
{
	U,  U,  U,  U,   U,  U,  U,  U,   /* NUL SOH STX ETX  EOT ENQ ACK BEL */
	U,  U,  U,  U,   U,  U,  U,  U,   /* BS  HT  LF  VT   FF  CR  SO  SI  */
	U,  U,  U,  U,   U,  U,  U,  U,   /* DLE DC1 DC2 DC3  DC4 NAK SYN ETB */
	U,  U,  U,  U,   U,  U,  U,  U,   /* CAN EM  SUB ESC  FS  GS  RS  US  */
	U,  0,  U, RU,   R,  U,  R,  0,   /* SP  !   "   #    $   %   &   '   */
	0,  0,  0,  R,   R,  0,  0,  R,   /* (   )   *   +    ,   -   .   /   */
	0,  0,  0,  0,   0,  0,  0,  0,   /* 0   1   2   3    4   5   6   7   */
	0,  0, RU,  R,   U,  R,  U,  R,   /* 8   9   :   ;    <   =   >   ?   */
	RU, 0,  0,  0,   0,  0,  0,  0,   /* @   A   B   C    D   E   F   G   */
	0,  0,  0,  0,   0,  0,  0,  0,   /* H   I   J   K    L   M   N   O   */
	0,  0,  0,  0,   0,  0,  0,  0,   /* P   Q   R   S    T   U   V   W   */
	0,  0,  0, RU,   U, RU,  U,  0,   /* X   Y   Z   [    \   ]   ^   _   */
	U,  0,  0,  0,   0,  0,  0,  0,   /* `   a   b   c    d   e   f   g   */
	0,  0,  0,  0,   0,  0,  0,  0,   /* h   i   j   k    l   m   n   o   */
	0,  0,  0,  0,   0,  0,  0,  0,   /* p   q   r   s    t   u   v   w   */
	0,  0,  0,  U,   U,  U,  0,  U,   /* x   y   z   {    |   }   ~   DEL */

	U, U, U, U,  U, U, U, U,  U, U, U, U,  U, U, U, U,
	U, U, U, U,  U, U, U, U,  U, U, U, U,  U, U, U, U,
	U, U, U, U,  U, U, U, U,  U, U, U, U,  U, U, U, U,
	U, U, U, U,  U, U, U, U,  U, U, U, U,  U, U, U, U,

	U, U, U, U,  U, U, U, U,  U, U, U, U,  U, U, U, U,
	U, U, U, U,  U, U, U, U,  U, U, U, U,  U, U, U, U,
	U, U, U, U,  U, U, U, U,  U, U, U, U,  U, U, U, U,
	U, U, U, U,  U, U, U, U,  U, U, U, U,  U, U, U, U,
};
#undef R
#undef U
#undef RU

/* URL-unescape the string S.

   This is done by transforming the sequences "%HH" to the character
   represented by the hexadecimal digits HH.  If % is not followed by
   two hexadecimal digits, it is inserted literally.

   The transformation is done in place.  If you need the original
   string intact, make a copy before calling this function.  */

void url_unescape (char *s)
{
	char *t = s;                  /* t - tortoise */
	char *h = s;                  /* h - hare     */

	for (; *h; h++, t++)
	{
		if (*h != '%')
		{
copychar:
			*t = *h;
		}
		else
		{
			char c;
			/* Do nothing if '%' is not followed by two hex digits. */
			if (!h[1] || !h[2] || !(isxdigit (h[1]) && isxdigit (h[2])))
				goto copychar;
			c = X2DIGITS_TO_NUM (h[1], h[2]);
			/* Don't unescape %00 because there is no way to insert it
			   into a C string without effectively truncating it. */
			if (c == '\0')
				goto copychar;
			*t = c;
			h += 2;
		}
	}
	*t = '\0';
}

void url_decode( char *s )
{
	char *t = s;                  /* t - tortoise */
	char *h = s;                  /* h - hare     */

	for (; *h; h++, t++)
	{
		if (*h == '+' )
		{
			*t = ' ';
		}
		else if (*h != '%')
		{
copychar:
			*t = *h;
		}
		else
		{
			char c;

			/* Do nothing if '%' is not followed by two hex digits. */
			if (!h[1] || !h[2] || !(isxdigit (h[1]) && isxdigit (h[2])))
				goto copychar;
			c = X2DIGITS_TO_NUM (h[1], h[2]);
			/* Don't unescape %00 because there is no way to insert it
			   into a C string without effectively truncating it. */
			if (c == '\0')
				goto copychar;
			*t = c;
			h += 2;
		}
	}
	*t = '\0';
}

/* The core of url_escape_* functions.  Escapes the characters that
   match the provided mask in urlchr_table.

   If ALLOW_PASSTHROUGH is TRUE, a string with no unsafe chars will be
   returned unchanged.  If ALLOW_PASSTHROUGH is FALSE, a freshly
   allocated string will be returned in all cases.  */
char *url_escape_1 (const char *s, unsigned char mask, boolean allow_passthrough)
{
	const char *p1;
	char *p2, *newstr;
	int newlen;
	int addition = 0;

	for (p1 = s; *p1; p1++)
		if (urlchr_test (*p1, mask))
			addition += 2;            /* Two more characters (hex digits) */

	if (!addition)
		return allow_passthrough ? (char *)s : strdup (s);

	newlen = (p1 - s) + addition;
	newstr = malloc (newlen + 1);

	p1 = s;
	p2 = newstr;
	while (*p1)
	{
		/* Quote the characters that match the test mask. */
		if (urlchr_test (*p1, mask))
		{
			unsigned char c = *p1++;
			*p2++ = '%';
			*p2++ = XNUM_TO_DIGIT (c >> 4);
			*p2++ = XNUM_TO_DIGIT (c & 0xf);
		}
		else
			*p2++ = *p1++;
	}
	assert (p2 - newstr == newlen);
	*p2 = '\0';

	return newstr;
}

/* URL-escape the unsafe characters (see urlchr_table) in a given
   string, returning a freshly allocated string.  */
char *url_escape (const char *s)
{
	return url_escape_1 (s, urlchr_unsafe, FALSE);
}

/* URL-escape the unsafe characters (see urlchr_table) in a given
   string.  If no characters are unsafe, S is returned.  */
char *url_escape_allow_passthrough (const char *s)
{
	return url_escape_1 (s, urlchr_unsafe, TRUE);
}

/* Decide whether the char at position P needs to be encoded.  (It is
   not enough to pass a single char *P because the function may need
   to inspect the surrounding context.)

   Return TRUE if the char should be escaped as %XX, FALSE otherwise.  */

static inline boolean char_needs_escaping (const char *p)
{
	if (*p == '%')
	{
		if (isxdigit (*(p + 1)) && isxdigit (*(p + 2)))
			return FALSE;
		else
			/* Garbled %.. sequence: encode `%'. */
			return TRUE;
	}
	else if (URL_UNSAFE_CHAR (*p) && !URL_RESERVED_CHAR (*p))
		return TRUE;
	else
		return FALSE;
}

