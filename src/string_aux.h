#ifndef _STRING_AUX_H_
#define _STRING_AUX_H_

#include <libcollections/types.h>
#include <string.h>

/**
* Copy part of a string.
* Copy  count characters from src, starting from start
* @param src String to copy from
* @param start Initial offset 
* @param count Number of chars to copy
* @return The new string
* 
* \code
* char *part, *str = "Test one, test two";
* part = substr(str, 1, 5);
* puts(part); // -> est o
* \endcode
**/
char *substr(char *src, const int start, size_t count);


#ifndef strndup
char* strndup( const char *s, size_t len );
#endif

#endif /* _STRING_AUX_H_ */
