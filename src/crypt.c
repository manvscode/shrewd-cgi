/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <mcrypt.h>
#include "cgi.h"

static void *create_mcrypt_iv( MCRYPT td )
{ 
	char *iv; 
	size_t i; 

	iv = (char *) malloc( mcrypt_enc_get_iv_size( td ) ); 

	printf( "-----> sz = %ld\n", mcrypt_enc_get_iv_size( td ) );

	if( iv ) 
	{ 
		srand( clock( ) ); 
		for( i = 0; i < mcrypt_enc_get_iv_size( td ); i++ ) 
		{ 
			iv[ i ] = rand( ); 
		} 
	} 

	return iv; /* this may be NULL */
}



boolean cgi_encrypt( char *message, size_t message_len, const char *key, size_t key_len )
{
	MCRYPT td;
	void *iv;
	int error = 0;
	size_t blocksize = 0;

	td = mcrypt_module_open( MCRYPT_RIJNDAEL_256, "", MCRYPT_CFB, "" );

	if( td == MCRYPT_FAILED )
	{
		cgi_error( E_WARNING, "Failed to open mcrypt module for %s. (%s:%ld)\n", MCRYPT_RIJNDAEL_256, __FILE__, __LINE__ );
		return FALSE;
	}

	blocksize = mcrypt_enc_get_block_size( td );
	assert( message_len >= blocksize );

	iv = create_mcrypt_iv( td );

	if( !iv ) 
	{ 
		cgi_error( E_MEMORY, "Failed to allocate memory. (%s:%ld)\n", __FILE__, __LINE__ ); 
		mcrypt_module_close( td ); 
		return FALSE; 
	} 

	error = mcrypt_generic_init( td, (void *) key, key_len, iv );

	if( error != 0 )
	{
		free( iv );
		mcrypt_module_close( td );

		const char *error_msg = mcrypt_strerror( error );
		cgi_error( E_WARNING, "%s\b\b (%s:%ld)\n", error_msg, __FILE__, __LINE__ );
		return FALSE;
	}

	size_t bytes_encoded = 0;	

	while( bytes_encoded < message_len )
	{
		mcrypt_generic( td, message + bytes_encoded, blocksize );
		bytes_encoded += blocksize;
		/* mdecrypt_generic (td, block_buffer, blocksize); */
	}

	free( iv );
	mcrypt_generic_deinit( td );
	mcrypt_module_close( td );

	return TRUE;
}


boolean cgi_decrypt( char *message, size_t message_len, const char *key, size_t key_len )
{
	MCRYPT td;
	void *iv;
	int error = 0;

	td = mcrypt_module_open( MCRYPT_RIJNDAEL_256, "", MCRYPT_CFB, "" );

	if( td == MCRYPT_FAILED )
	{
		cgi_error( E_WARNING, "Failed to open mcrypt module for %s. (%s:%ld)\n", MCRYPT_RIJNDAEL_256, __FILE__, __LINE__ );
		return FALSE;
	}

	iv = create_mcrypt_iv( td );

	if( !iv )
	{
		cgi_error( E_MEMORY, "Failed to allocate memory. (%s:%ld)\n", __FILE__, __LINE__ );
		mcrypt_module_close( td );
		return FALSE;
	}

	error = mcrypt_generic_init( td, (void *) key, key_len, iv );

	if( error != 0 )
	{
		free( iv );
		mcrypt_module_close( td );

		const char *error_msg = mcrypt_strerror( error );
		cgi_error( E_WARNING, "%s (%s:%ld)\n", error_msg, __FILE__, __LINE__ );
		return FALSE;
	}	

	error = mdecrypt_generic( td, message, message_len );

	if( error != 0 )
	{
		free( iv );
		mcrypt_generic_deinit( td );
		mcrypt_module_close( td );

		const char *error_msg = mcrypt_strerror( error );
		cgi_error( E_WARNING, "%s (%s:%ld)\n", error_msg, __FILE__, __LINE__ );
		return FALSE;
	}

	free( iv );
	mcrypt_generic_deinit( td );
	mcrypt_module_close( td );

	return TRUE;

}

