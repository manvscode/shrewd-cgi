/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdlib.h>
#include "http_status.h"

http_status http_status_table[ HTTP_STATUS_TABLE_SIZE ] = {
	{ 100, "Continue", NULL },
	{ 101, "Switching Protocols", NULL },
	{ 102, "Processing", NULL },
	{ 200, "OK", NULL }, /* this is first, because we need to find it 99% of the time */
	{ 201, "Created", NULL },
	{ 202, "Accepted", NULL },
	{ 203, "Non-Authoritative Information", NULL },
	{ 204, "No Content", NULL },
	{ 205, "Reset Content", NULL },
	{ 206, "Partial Content", NULL },
	{ 207, "Multi-Status", NULL },
	{ 300, "Multiple Choices", NULL },
	{ 301, "Moved Permanently", NULL },
	{ 302, "Found", NULL },
	{ 303, "See Other", NULL },
	{ 304, "Not Modified", NULL },
	{ 305, "Use Proxy", NULL },
	{ 306, "Switch Proxy", NULL },
	{ 307, "Temporary Redirect", NULL },
	{ 400, "Bad Request", "The specified request is invalid." },
	{ 401, "Unauthorized", "Unauthorized to access the document." },
	{ 402, "Payment Required", NULL },
	{ 403, "Forbidden", "Server refused to fulfill your request." },
	{ 404, "Not Found", "The server cannot find the requested resource or page." },
	{ 405, "Method Not Allowed", NULL },
	{ 406, "Not Acceptable", NULL },
	{ 407, "Proxy Authentication Required", NULL },
	{ 408, "Request Time-out", "The request was timed-out." },
	{ 409, "Conflict", NULL },
	{ 410, "Gone", "The resource you requested is permanently no-longer available." },
	{ 411, "Length Required", NULL },
	{ 412, "Precondition Failed", NULL },
	{ 413, "Request Entity Too Large", NULL },
	{ 414, "Request-URI Too Large", NULL },
	{ 415, "Unsupported Media Type", NULL },
	{ 416, "Requested Range Not Satisfiable", NULL },
	{ 417, "Expectation Failed", NULL },
	{ 418, "I'm a teapot", NULL },
	{ 422, "Unprocessable Entity", "The request was well-formed but was unable to be followed due to semantic errors." },
	{ 423, "Locked", NULL },
	{ 424, "Failed Dependency", NULL },
	{ 425, "Unordered Collection", NULL },
	{ 426, "Upgrade Required", "The client should switch to a different protocol such as TLS/1.0." },
	{ 449, "Retry With", NULL },
	{ 450, "Blocked by Windows Parental Controls", NULL },
	{ 500, "Internal Server Error", NULL },
	{ 501, "Not Implemented", "The requested method is not implemented by this server." },
	{ 502, "Bad Gateway", NULL },
	{ 503, "Service Unavailable", NULL },
	{ 504, "Gateway Timeout", NULL },
	{ 505, "HTTP Version Not Supported", "This server doesn't support the specified HTTP version." },
	{ 506, "Variant Also Negotiates", NULL },
	{ 507, "Insufficient Storage", NULL },
	{ 509, "Bandwidth Limit Exceeded", NULL },
	{ 510, "Not Extended", NULL },
};


int http_status_compare( const void *left, const void *right ) 
{
	http_status *p_left  = (http_status *) left;
	http_status *p_right = (http_status *) right;

	return p_left->status_code - p_right->status_code;
}

const char *http_status_reason( int code ) 
{
	http_status key;
	key.status_code = code;

	http_status *p_status = bsearch( &key, http_status_table, HTTP_STATUS_TABLE_SIZE, sizeof(http_status), http_status_compare );
	
	return p_status ? p_status->reason_phrase : NULL;
}

const char *http_status_long_description( int code ) 
{
	http_status key;
	key.status_code = code;

	http_status *p_status = bsearch( &key, http_status_table, HTTP_STATUS_TABLE_SIZE, sizeof(http_status), http_status_compare );
	
	return p_status ? p_status->long_description : NULL;
}
