/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <assert.h>
#include <libcollections/flat-db.h>
#include <libcollections/tree-map.h>
#include <libcollections/hash-functions.h>
#include "session-lexer.h"
#include "session-parser.h"
#include "cgi.h"


#define VERBOSE_SESSIONS

#define SESSION_DATABASE    "./sessions.db"
#define SESSIONS            0
	
flatdb_t session_db;	

extern int session_parse( void );


static char*       cgi_session_id_create    ( void );

static size_t  session_hash        ( const flat_record *p_record );
static int     session_compare     ( const flat_record *p_left, const flat_record *p_right );
static boolean session_var_destroy ( void *p_key, void *p_value );

#pragma pack(push, 1)
typedef struct _session_record {
	flat_record base; // 12 bytes
	cgi_session session;
} session_record;
#pragma pack(pop)

static session_record session_rec;
tree_map_t       session_data;

boolean cgi_session_initialize( void )
{
	session_db = flatdb_open( SESSION_DATABASE );

	if( !session_db )
	{
		session_db = flatdb_create( SESSION_DATABASE, 1 /* max tables */, FLDB_MAX_RECORDS );

		flat_table *p_table = flatdb_table_get( session_db, SESSIONS );

		if( p_table )
		{
			/* Initialize SESSIONS table */
			flat_object_unset( p_table, FLDB_UNUSED );
			p_table->record_size  = sizeof(session_record);
			p_table->reserved     = 0;

			flatdb_table_save( session_db, SESSIONS );
		}
	
		memset( &session_rec, 0, sizeof(session_record) );	
		cgi.session = NULL;
	}

	flatdb_record_hasher   ( session_db, SESSIONS, session_hash );
	flatdb_record_comparer ( session_db, SESSIONS, session_compare );

	tree_map_create( &session_data, session_var_destroy, (tree_map_compare_function) strcmp, malloc, free );

	return session_db != NULL;
}

boolean cgi_session_deinitialize( void )
{
	tree_map_destroy( &session_data );
	flatdb_close( &session_db );

	return session_db == NULL;
}

char* cgi_session_id_create( )
{
	#if defined(SESSION_ID_INCLUDES_REMOTE_USER)
	const char* remote_user    = cgi_remote_user( ); 
	#else
	const char *remote_user    = "";
	#endif
	const char* remote_address = cgi_remote_address( ); /* max 10 chars */
	const char* application    = cgi_app( );

	char buffer[ 256 ];
	snprintf( buffer, sizeof(buffer), "%s(%s@%s)", application, remote_user, remote_address );
	buffer[ sizeof(buffer) - 1 ] = '\0';

	char *session_id = cgi_sha256( buffer, strlen(buffer) );

	return session_id;
}

void cgi_session_start( void )
{
	flat_id_t found_id;

	const char *session_id = cgi_session_id_create( );

	strncpy( session_rec.session.id, session_id, sizeof(session_rec.session.id) );
	session_rec.session.id[ sizeof(session_rec.session.id) - 1 ] = '\0';
	strncpy( session_rec.session.ip, cgi_remote_address( ), sizeof(session_rec.session.ip) );
	session_rec.session.ip[ sizeof(session_rec.session.ip) - 1 ] = '\0';
	
	tree_map_clear( &session_data );

	if( !flatdb_record_search( session_db, SESSIONS, (flat_record *) &session_rec, &found_id ) )
	{
		/* Create a new session */
		session_rec.session.data[ 0 ] = '\0';
		session_rec.session.created   = time( NULL );

		cgi.session = &session_rec.session;
	
		#if defined(VERBOSE_SESSIONS)
		cgi_out( "<strong> [Shrewd-CGI] </strong> Created session record. <br />" );
		#endif
		cgi_log( E_INFO, "Created session record (id = %s, ip = %s).\n", session_id, cgi_remote_address() );
	
		flatdb_record_add( session_db, SESSIONS, (flat_record *) &session_rec );
	}
	else
	{
		/*
 		 * If session has not expired, then initialize session.
 		 * Otherwise, destroy the session.
 		 */
		time_t now = time( NULL );

		if( now >= (session_rec.session.created + CGI_SESSION_EXPIRATION_SECONDS) )
		{
			session_rec.session.data[ 0 ] = '\0';
			session_rec.session.created   = time( NULL );

			#if defined(VERBOSE_SESSIONS)
			cgi_out( "<strong> [Shrewd-CGI] </strong> Session expired. <br />" );
			#endif
			cgi_log( E_INFO, "Session expired (id = %s, ip = %s).\n", session_id, cgi_remote_address() );
		}
		else
		{
			#if defined(VERBOSE_SESSIONS)
			cgi_out( "<strong> [Shrewd-CGI] </strong> Loaded session record. <br />" );
			#endif
			cgi_log( E_INFO, "session buffer [%s]\n", session_rec.session.data );

			// Load session data into tree map from flatdb.
			#if 1
			YY_BUFFER_STATE yybs = session__scan_string( session_rec.session.data );
			session_parse( );
			session__delete_buffer( yybs );
			#else
			YY_BUFFER_STATE yybs = session__scan_buffer( session_rec.session.data, sizeof(session_rec.session.data) );
			session_parse( );
			session__delete_buffer( yybs );
			#endif

			cgi_log( E_INFO, "Loaded session record (id = %s, ip = %s, items = %ld).\n", session_id, cgi_remote_address(), tree_map_size(&session_data) );
		}
		
		cgi.session = &session_rec.session;
	}



	free( (void *) session_id );	
}

void cgi_session_end( void )
{
	#if defined(VERBOSE_SESSIONS)
	cgi_out( "<strong> [Shrewd-CGI] </strong> Saving session record. <br />" );
	#endif
	cgi_log( E_INFO, "Saving session record (id = %s, ip = %s, items = %ld).\n", session_rec.session.id, session_rec.session.ip, tree_map_size(&session_data) );
	tree_map_iterator_t itr;
	static char buffer[ 1 * KILOBYTE ];

	const size_t SIZE = sizeof(session_rec.session.data);
	size_t position   = 0;


	session_rec.session.data[ 0 ] = '\0';

	for( itr = tree_map_begin( &session_data ); 
	     itr != tree_map_end( ) && position < SIZE; 
	     itr = tree_map_next(itr) )
	{
		// the parser is expecting key=value\r\n\r\n
		int chars_written = snprintf( buffer, sizeof(buffer), "%s=%s\r\n\r\n", (char *) itr->key, (char *) itr->value );
		buffer[ sizeof(buffer) - 1 ] = '\0';

		if( chars_written > 0 )
		{
			void *start = session_rec.session.data + position;

			memcpy( start, buffer, chars_written );
			position += chars_written;
		}
	}
	buffer[ position + 1 ] = '\0';

	flatdb_record_save( session_db, SESSIONS, (flat_record *) &session_rec );
}

void cgi_session_destroy( void )
{
	flat_id_t found_id;
	const char *session_id = cgi_session_id_create( );

	session_record key;
	strncpy( session_rec.session.id, session_id, sizeof(session_rec.session.id) );
	strncpy( session_rec.session.ip, cgi_remote_address( ), sizeof(session_rec.session.ip) );

	if( flatdb_record_search( session_db, SESSIONS, (flat_record *) &key, &found_id ) )
	{
		cgi_log( E_INFO, "Session destroyed (id = %s).\n", session_id );

		flatdb_record_delete( session_db, SESSIONS, found_id );
		cgi.session = NULL;
	}
	else
	{
		cgi_error( E_WARNING, "Unable to find session record to delete." );
		cgi_log( E_WARNING, "Unable to find session record to delete (id = %s).\n", session_id );
	}

	free( (void *) session_id );	
}

boolean cgi_session_set( const char *key, const char *value )
{
	cgi_log( E_WARNING, "Session set (id = %s, key = %s, value = %s).\n", session_rec.session.id, key, value );
	return tree_map_insert( &session_data, strdup(key), strdup(value) );
}

boolean session_parser_set( const char *key, const char *value )
{
	return tree_map_insert( &session_data, key, value );
}

boolean cgi_session_setf( const char *key, const char *value_format, ... )
{
	char value_buffer[ 512 ];
	va_list args;

	va_start( args, value_format );
	vsnprintf( value_buffer, sizeof(value_buffer), value_format, args );
	value_buffer[ sizeof(value_buffer) - 1 ] = '\0';
	va_end( args );

	cgi_log( E_WARNING, "Session set (id = %s, key = %s, value = %s).\n", session_rec.session.id, key, value_buffer );
	return tree_map_insert( &session_data, strdup(key), strdup(value_buffer) );
}

const char* cgi_session_get( const char *key )
{
	char *value = NULL;

	if( tree_map_find( &session_data, key, (void **) &value ) )
	{
		cgi_log( E_WARNING, "Session get (id = %s, key = %s).\n", session_rec.session.id, key );
		return value;	
	}

	cgi_log( E_WARNING, "Session get not found (id = %s, key = %s).\n", session_rec.session.id, key );
	return NULL;
}

boolean cgi_session_delete( const char *key )
{
	cgi_log( E_WARNING, "Session delete (id = %s, key = %s).\n", session_rec.session.id, key );
	return tree_map_remove( &session_data, key );
}

size_t session_hash( const flat_record *p_rec )
{
	const session_record *p_record = (session_record *) p_rec;

	assert( p_rec );
	unsigned short count = 0;
	size_t hash = 0;

	char ip[ 24 ];
	strncpy( ip, p_record->session.ip, sizeof(ip) );
	ip[ sizeof(ip) - 1 ] = '\0';

	char *token = strtok( (char *) ip, "." );

	while( token != NULL )
	{
		int part = atoi( token );

		hash |= (part << 8 * (3 - count));
	
		token = strtok( NULL, "." );
	}

	return hash;
}

int session_compare( const flat_record *p_left, const flat_record *p_right )
{
	const session_record *p_left_session  = (session_record *) p_left;	
	const session_record *p_right_session = (session_record *) p_right;	

	return strcmp( p_left_session->session.id, p_right_session->session.id );
}

boolean session_var_destroy ( void *p_key, void *p_value )
{
	free( p_key );
	free( p_value );
	return TRUE;
}

