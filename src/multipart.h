/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _MULTIPART_H_
#define _MULTIPART_H_

#include <stdlib.h>
#include <libcollections/types.h>

/*
 *  Multipart/form-data Token Types
 */	
typedef enum enum_multipart_token_type {
	TOK_END = 0,
	TOK_DATA,
	TOK_BOUNDARY,
	TOK_BOUNDARY_END,
	TOK_CONTENT_DISPOSITION_HEADER,
	TOK_CONTENT_TYPE_HEADER,
	TOK_CONTENT_TRANSFER_ENCODING_HEADER,
	TOK_FORM_DATA,
	TOK_NAME,
	TOK_FILENAME,
	TOK_EQUAL,
	TOK_QUOTE,
	TOK_SEMICOLON,
	TOK_CRLF,
	MULTIPART_TOKEN_COUNT
} multipart_token_type;

#define TOK_UNDEFINED (TOK_END)

/*
 *  Multipart/form-data Token
 */	
typedef struct struct_multipart_token {
	multipart_token_type type;
	const char*          lexeme;
	size_t               length;
} multipart_token;

/*
 *  Multipart/form-data Lexer
 */	
typedef struct struct_multipart_lexer {
	const char*     buffer;
	size_t          buffer_size;
	char*           boundary;
	size_t          boundary_length;
	char*           end_boundary;
	size_t          end_boundary_length;
	size_t          position;
	multipart_token token;
	boolean         is_file; 
	boolean         reading_file; 
} multipart_lexer;

/*
 *  Multipart/form-data Lexer Functions
 */	
void                 multipart_lexer_create( multipart_lexer *p_lexer, const char *p_buffer, size_t buffer_size, const char *boundary );
void                 multipart_lexer_destroy( multipart_lexer *p_lexer );
const char*          multipart_lexer_lookup_token( multipart_token_type token_type );
const char*          multipart_lexer_token_string( multipart_token_type token_type );
multipart_token_type multipart_lexer_next_token( multipart_lexer *p_lexer );
#define              multipart_lexer_advance( p_lexer )              (multipart_lexer_next_token(p_lexer))
#define              multipart_lexer_token_type( p_lexer )           ((p_lexer)->token.type) 
#define              multipart_lexer_token_lexeme( p_lexer )         ((p_lexer)->token.lexeme) 
#define              multipart_lexer_token_lexeme_length( p_lexer )  ((p_lexer)->token.length) 

/*
 *  Multipart/form-data Parser
 */	
typedef struct struct_multipart_parser {
	multipart_token name;
	multipart_token filename;
	multipart_token content_type;
	multipart_token encoding;
	multipart_token data;
} multipart_parser;

/*
 *  Multipart/form-data Parser Functions
 */	
boolean multipart_parser_eat            ( multipart_lexer *p_lexer, multipart_token_type token );
void    multipart_parser_eate           ( multipart_lexer *p_lexer, multipart_token_type token );
void    multipart_parser_handle_part    ( multipart_parser *p_parser, multipart_lexer *p_lexer );
void    multipart_parser_handle_header  ( multipart_parser *p_parser, multipart_lexer *p_lexer );

#endif /* _MULTIPART_H_ */
