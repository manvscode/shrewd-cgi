/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifdef FCGI_BUILD
#include <fcgi_stdio.h> /* fcgi library; put it first*/
#endif
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include "cgi.h"

boolean cgi_cookie_set( const char *key, const char *value, time_t expires, boolean is_max_age )
{
	assert( key && *key );
	assert( value );

	if( key == NULL || *key == '\0' )
	{
		return FALSE;
	}

	if( expires != 0L )
	{
		cgi_headerf( FALSE, "Set-Cookie: %s=%s", key, value );
	}
	else
	{
		if( is_max_age )
		{
			cgi_headerf( FALSE, "Set-Cookie: %s=%s; %s=%ld", key, value, CGI_COOKIE_ATTRIBUTE_MAX_AGE, expires );
		}
		else
		{
			char time_string[ 128 ];
			struct tm *p_tm = gmtime( &expires );
			strftime( time_string, sizeof(time_string), CGI_DATETIME_FORMAT, p_tm );
			cgi_headerf( FALSE, "Set-Cookie: %s=%s; %s=%s", key, value, CGI_COOKIE_ATTRIBUTE_EXPIRES, time_string );
		}
	}

	return TRUE;
}

boolean cgi_cookie_setf( const char *key, const char *value, time_t expires, boolean is_max_age, const char *attribute_format, ... )
{
	assert( key && *key );
	assert( value );

	if( key == NULL || *key == '\0' )
	{
		return FALSE;
	}

	if( expires != 0L )
	{
		va_list args;
		va_start( args, attribute_format );
		char attributes[ 512 ];
		vsnprintf( attributes, sizeof(attributes), attribute_format, args );
		attributes[ sizeof(attributes) - 1 ] = '\0';
		cgi_headerf( FALSE, "Set-Cookie: %s=%s; %s", key, value, attributes );
		va_end( args );
	}
	else
	{
		if( is_max_age )
		{
			va_list args;
			va_start( args, attribute_format );
			char attributes[ 512 ];
			vsnprintf( attributes, sizeof(attributes), attribute_format, args );
			attributes[ sizeof(attributes) - 1 ] = '\0';
			cgi_headerf( FALSE, "Set-Cookie: %s=%s; %s=%ld; %s", key, value,  CGI_COOKIE_ATTRIBUTE_MAX_AGE, expires, attributes );
			va_end( args );
		}
		else
		{
			va_list args;
			char attributes[ 512 ];
			char time_string[ 128 ];
			struct tm *p_tm = gmtime( &expires );

			strftime( time_string, sizeof(time_string), CGI_DATETIME_FORMAT, p_tm );
			va_start( args, attribute_format );
			vsnprintf( attributes, sizeof(attributes), attribute_format, args );
			attributes[ sizeof(attributes) - 1 ] = '\0';
			cgi_headerf( FALSE, "Set-Cookie: %s=%s; %s=%s; %s", key, value,  CGI_COOKIE_ATTRIBUTE_EXPIRES, time_string, attributes );
			va_end( args );
		}
	}

	return TRUE;
}

boolean cgi_cookie_setf_ex( const char *key, const char *value, const char *attribute_format, ... )
{
	assert( key && *key );
	assert( value );

	if( key == NULL || *key == '\0' )
	{
		return FALSE;
	}

	if( attribute_format )
	{
		va_list args;
		va_start( args, attribute_format );
		char attributes[ 512 ];
		vsnprintf( attributes, sizeof(attributes), attribute_format, args );
		attributes[ sizeof(attributes) - 1 ] = '\0';
		cgi_headerf( FALSE, "Set-Cookie: %s=%s; %s", key, value, attributes );
		va_end( args );
	}
	else
	{
		cgi_headerf( FALSE, "Set-Cookie: %s=%s", key, value );
	}

	return TRUE;
}

const char *cgi_cookie_get( const char *key )
{
	char *value       = NULL;	
	boolean key_found = hash_map_find( &cgi.cookies, key, (void**) &value ); 

	if( key_found )
	{
		assert( value );
		return value;
	}

	return NULL;
}

boolean cgi_cookie_delete( const char *key )
{
	return hash_map_remove( &cgi.cookies, key );
}
