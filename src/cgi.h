/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _CGI_H_
#define _CGI_H_

#ifdef FCGI_BUILD
#include <fcgi_stdio.h> /* fcgi library; put it first*/
	#define fcgi_begin( ) \
		while( FCGI_Accept() >= 0 ) { 
	#define fcgi_end( ) \
		}
#else
	#define fcgi_begin( ) 
	#define fcgi_end( ) 
#endif

#include <stdarg.h>
#include <time.h>
#include <libcollections/types.h>
#include <libcollections/hash-table.h>
#include <libcollections/hash-map.h>
#include <libcollections/rbtree.h>
#include <libcollections/tstring.h>
#include <mhash.h>
#include "buffer.h"
#include "url.h"

typedef unsigned short flags_t;

/*
 * 	CGI Options
 */
#define CGI_OPT_DISPLAY_ERRORS             (0x0001)
#define CGI_OPT_ENABLE_LOGGING             (0x0002)
#define CGI_OPT_ENABLE_SESSIONS            (0x0004)

/*
 * 	CGI State Flags
 */
#define CGI_STS_HEADERS_SENT               (0x1000)
#define CGI_STS_SESSION_STARTED            (0x2000)


#define CGI_DEFAULT_OPTIONS                (CGI_OPT_ENABLE_LOGGING)
#define CGI_APP_NAME_SIZE                  (128)
#define CGI_MAX_PATH_PARTS                 (32)
#define KILOBYTE                           (1024) /* One megabyte in bytes */
#define MEGABYTE                           (1048576) /* One megabyte in bytes */
#define CGI_DATETIME_FORMAT                "%a, %d-%b-%Y %H:%M:%S %Z" /* Wdy, DD-Mon-YYYY HH:MM:SS GMT */
#ifndef CGI_MAX_CONTENT_LENGTH
#define CGI_MAX_CONTENT_LENGTH             (8 * MEGABYTE)
#endif
#ifndef CGI_POST_BUFFER_SIZE
#define CGI_POST_BUFFER_SIZE               (CGI_MAX_CONTENT_LENGTH)
#endif
#ifndef CGI_SESSION_BUFFER_SIZE
#define CGI_SESSION_BUFFER_SIZE            (8 * KILOBYTE)
#endif
#ifndef CGI_OUTPUT_BUFFER_SIZE
#define CGI_OUTPUT_BUFFER_SIZE             (8 * MEGABYTE)
#endif

#define TBLSIZE_00                         (37)	
#define TBLSIZE_01                         (59)	
#define TBLSIZE_02                         (79)	
#define TBLSIZE_03                         (109)	
#define TBLSIZE_04                         (149)	
#define TBLSIZE_05                         (199)	
#define TBLSIZE_06                         (269)	
#define TBLSIZE_07                         (349)	
#define TBLSIZE_08                         (389)	
#define TBLSIZE_09                         (449)	
#define TBLSIZE_10                         (499)	
#define TBLSIZE_11                         (1031)	
#define TBLSIZE_12                         (5153)	
#define TBLSIZE_13                         (7919)	

#ifndef CGI_HEADER_TBLSIZE
#define CGI_HEADER_TBLSIZE                 TBLSIZE_00
#endif
#ifndef CGI_PARAM_TBLSIZE
#define CGI_PARAM_TBLSIZE                  TBLSIZE_01
#endif
#ifndef CGI_FILE_TBLSIZE
#define CGI_FILE_TBLSIZE                   TBLSIZE_00
#endif
#ifndef CGI_COOKIE_TBLSIZE
#define CGI_COOKIE_TBLSIZE                 TBLSIZE_01
#endif
#ifndef CGI_SESSION_TBLSIZE
#define CGI_SESSION_TBLSIZE                TBLSIZE_07
#endif

#define CGI_COOKIE_ATTRIBUTE_EXPIRES       "expires"
#define CGI_COOKIE_ATTRIBUTE_MAX_AGE       "max-age"
#define CGI_COOKIE_ATTRIBUTE_DOMAIN        "domain"
#define CGI_COOKIE_ATTRIBUTE_PATH          "path"
#define CGI_COOKIE_ATTRIBUTE_SECURE        "secure"
#define CGI_COOKIE_ATTRIBUTE_HTTPONLY      "httponly"

#define CGI_MINUTE                         (60)
#define CGI_HOUR                           (60 * CGI_MINUTE)
#define CGI_SESSION_EXPIRATION_SECONDS     (30 * CGI_MINUTE)


#define POST_FILL( p_record, member ) \
	const char *__tmp_##member = cgi_post( #member ); \
	if( __tmp_##member ) \
	{ \
		strcpy( (p_record)->member,  __tmp_##member ); \
		url_unescape( (p_record)->member ); \
		modified = TRUE; \
	} 

#define POST_FILL_EX( p_record, member, size ) \
	const char *__tmp_##member = cgi_post( #member ); \
	if( __tmp_##member ) \
	{ \
		strncpy( (p_record)->member,  __tmp_##member, size ); \
		(p_record)->member[ size - 1 ] = '\0'; \
		url_unescape( (p_record)->member ); \
		modified = TRUE; \
	} 

typedef enum enum_cgi_error_code {
	E_WARNING = 0,
	E_FATAL,
	E_CAUTION,
	E_INFO,
	E_MEMORY
} cgi_error_code;

typedef enum enum_request_method_type {
	UNDEFINED = 0,
	HEAD,
	GET,
	POST,
	PUT,
	DELETE,
	TRACE,
	OPTIONS,
	CONNECT
} request_method_type;

typedef struct struct_cgi_file {
	char *name;
	char *filename;
	char *type; /* content-type */
	FILE *tmpfile;
	size_t size;
} cgi_file;

typedef struct struct_cgi_session {
	char   id[ 128 ];      /* Session ID */
	char   ip[ 16 ];      /* IP Address */
	char   data[ CGI_SESSION_BUFFER_SIZE ];
	time_t created;
} cgi_session;

#define CGI_VALUE_INTEGER

typedef struct struct_cgi_value {
	flags_t type;	
	union {
		struct {
			char*  string;
			size_t len;
		};
		struct {
			byte*  buffer;
			size_t size;
		};
		long     n;
		double   d;
		boolean  b;
	};
} cgi_value;


typedef struct _cgi_path_info_parts {
	char *parts[ CGI_MAX_PATH_PARTS ];
	size_t count;
} cgi_path_info_parts;


typedef struct struct_cgi_handle {
	char application_name[ CGI_APP_NAME_SIZE ];
	char *domain;
	request_method_type request_method;	
	char post_buffer[ CGI_POST_BUFFER_SIZE ];

	
	#ifdef HEADERS_HASHED
	hash_table_t  response_headers;
	#else 
	rbtree_t      response_headers;
	#endif
	hash_map_t    get_variables;
	hash_map_t    post_variables;
	hash_map_t    cookies;
	hash_table_t  files;
	buffer        output_buffer;

	cgi_session   *session;

	cgi_path_info_parts path_info;
	flags_t       options;
	flags_t       state;
} cgi_handle;

extern cgi_handle cgi;



/*
 *  Main CGI Functions
 */
void            cgi_create          ( flags_t f, const char *application_name, const char *domain );
void            cgi_destroy         ( void );
const char*     cgi_app             ( void );
const char*     cgi_domain          ( void );
void            cgi_reset           ( void );
void            cgi_error           ( cgi_error_code error_code, const char *msg, ... );
void            cgi_process_request ( void );
boolean         cgi_headerf         ( boolean replace, char *header_fmt, ... );
#define         cgi_header(header)  (cgi_headerf( TRUE, header ))
void            cgi_out             ( const char *format, ... );
void            cgi_send_response   ( void );
void            cgi_redirect        ( const char *url );
void            cgi_redirectf       ( const char *url_fmt, ... );
const char*     cgi_get             ( const char *key );
const char*     cgi_post            ( const char *key );
const cgi_file* cgi_files           ( const char *key );
void            cgi_include         ( const char *filename );
const char*     cgi_urlf            ( const char *protocol, const char *hostname, const char *relative_url_format, ... );
#define         cgi_url( rel_url )  (cgi_urlf( NULL, NULL, rel_url ))



/*
 * Path Info 
 */
boolean         cgi_path_info_create  ( const char *path_info );
void            cgi_path_info_destroy ( void );

/*
 * Cookies 
 */
boolean     cgi_cookie_set      ( const char *key, const char *value, time_t expires, boolean is_max_age );
boolean     cgi_cookie_setf     ( const char *key, const char *value, time_t expires, boolean is_max_age, const char *attribute_format, ... );
boolean     cgi_cookie_setf_ex  ( const char *key, const char *value, const char *attribute_format, ... );
const char* cgi_cookie_get      ( const char *key );
boolean     cgi_cookie_delete   ( const char *key );

/*
 * Sessions
 */
void        cgi_session_destroy      ( void );
boolean     cgi_session_set          ( const char *key, const char *value );
boolean     cgi_session_setf         ( const char *key, const char *value_format, ... );
const char* cgi_session_get          ( const char *key );
boolean     cgi_session_delete       ( const char *key );

/*
 * Logging
 */
void cgi_log_initialize   ( void );
void cgi_log_deinitialize ( void );
void cgi_log              ( cgi_error_code error_code, const char *format, ... );
void cgi_vlog             ( cgi_error_code error_code, const char *format, va_list arguments );

/*
 * Hashing
 */

#define CGI_MD5_SIZE					(128 / 8)
#define CGI_SHA1_SIZE					(160 / 8)
#define CGI_SHA256_SIZE					(256 / 8)
#define CGI_SHA512_SIZE					(512 / 8)
boolean cgi_hash       ( hashid type, void *result, const void *plaintext, size_t size );
char*   cgi_bin2hex    ( void *hash, size_t size );
char*   cgi_md5        ( const void *plaintext, size_t size );
char*   cgi_sha1       ( const void *plaintext, size_t size );
char*   cgi_sha256     ( const void *plaintext, size_t size );
char*   cgi_sha512     ( const void *plaintext, size_t size );

/*
 * Encryption: TODO
 */
boolean cgi_encrypt      ( char *message, size_t message_len, const char *key, size_t key_len );
boolean cgi_decrypt      ( char *message, size_t message_len, const char *key, size_t key_len );

/*
 * CGI Environment Variables
 */
const char* cgi_server_software   ( void );
const char* cgi_server_name       ( void );
const char* cgi_server_protocol   ( void );
const char* cgi_server_port       ( void );
const char* cgi_server_admin      ( void );
const char* cgi_is_https          ( void );
const char* cgi_request_method    ( void );
const char* cgi_script_name       ( void );
const char* cgi_path_info         ( void ); /* The extra path information followin the script's path in the URL. */
const char* cgi_path_translated   ( void ); /* The PATH_INFO mapped onto DOCUMENT_ROOT. */
const char* cgi_query_string      ( void ); /* Contains query information passed via the calling URL, following a question mark after the script location. */
const char* cgi_request_uri       ( void );
const char* cgi_remote_host       ( void );
const char* cgi_remote_address    ( void );
const char* cgi_remote_user       ( void );
const char* cgi_remote_ident      ( void );
const char* cgi_remote_port       ( void );
const char* cgi_auth_type         ( void );
const char* cgi_content_type      ( void );
const char* cgi_content_length    ( void );
const char* cgi_http_host         ( void );
const char* cgi_http_accept       ( void );
const char* cgi_http_referer      ( void );
const char* cgi_http_user_agent   ( void );
const char* cgi_document_root     ( void );
const char* cgi_cookies           ( void );

/*
 * CGI Flags 
 */
#define cgi_is_enabled( flag, flags )      (((flags) & (flag)) != 0)
#define cgi_is_disabled( flag, flags )     (((flags) & (flag)) == 0)
#define cgi_isset( flag, flags )           (cgi_is_enabled(flag, flags))
#define cgi_toggle_bit( flag, flags )      ((flags) ^= (flag))
#define cgi_set_bit( flag, flags )         ((flags) |= (flag))
#define cgi_unset_bit( flag, flags )       ((flags) &= ~(flag))



#endif /* _CGI_H_ */
