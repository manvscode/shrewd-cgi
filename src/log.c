/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdarg.h>
#include <stdlib.h>
#include <assert.h>
#include <syslog.h>
#include "cgi.h"

const int cgi_sys_log_types[] = {
	LOG_WARNING, 
	LOG_EMERG,
	LOG_NOTICE,
	LOG_INFO,
	LOG_EMERG
};

void cgi_log_initialize( ) 
{
	if( cgi_is_enabled(CGI_OPT_ENABLE_LOGGING, cgi.options) )
	{
		assert( cgi.application_name );
		assert( *cgi.application_name );

		int log_options = LOG_PID | LOG_NDELAY | LOG_NOWAIT;
		#ifdef _DEBUG_CGI
		log_options |= LOG_PERROR;
		#endif

		openlog( cgi.application_name, log_options, LOG_DAEMON );
	}
}

void cgi_log_deinitialize( )
{
	if( cgi_is_enabled(CGI_OPT_ENABLE_LOGGING, cgi.options) )
	{
		closelog( );
	}
}

void cgi_log( cgi_error_code error_code, const char *format, ... )
{
	assert( format );
	assert( *format );

	if( cgi_is_enabled(CGI_OPT_ENABLE_LOGGING, cgi.options) )
	{
		va_list arguments;
		va_start( arguments, format );
		vsyslog( cgi_sys_log_types[ error_code ], format, arguments );
		va_end( arguments );
	}
}

void cgi_vlog( cgi_error_code error_code, const char *format, va_list arguments )
{
	assert( format );
	assert( *format );

	if( cgi_is_enabled(CGI_OPT_ENABLE_LOGGING, cgi.options) )
	{
		vsyslog( cgi_sys_log_types[ error_code ], format, arguments );
	}
}

