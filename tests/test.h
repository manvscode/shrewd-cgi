#ifndef _TEST_H_
#define _TEST_H_

#include <libcollections/types.h>

#define AUTH_SALT             ("-M^pc$ZIp~SU+bL#") 

#define ERROR(message) \
	cgi_header( "Error: " message ); \
	cgi_error( E_FATAL, message " (%s:%d)", __FILE__, __LINE__ );

#define VERIFY_PARAM(param) \
	if( !(param) ) {\
		cgi_header( "Error: " #param " is a required parameter!" ); \
		cgi_error( E_CAUTION, #param " is a required parameter!\n" ); \
	}

#ifdef _DEBUG
#define ASSERT(exp) \
	if( !(exp) ) {\
		const char *true_or_false = (exp) ? "TRUE" : "FALSE"; \
		cgi_headerf( TRUE, "Error: " #exp " evaluated to %s!", true_or_false ); \
		cgi_error( E_FATAL, #exp " evaluated to %s.  (%s:%d)", true_or_false, __FILE__, __LINE__ );\
	}
#else
#define ASSERT(exp)
#endif


void    handle_request ( );


boolean file_exists    ( const char *path );
void    stream_file    ( const char *filename );

#define ENSURE_EXISTENCE_AND_COPY(path, file_to_copy) \
	if( !file_exists( (path) ) ) { \
		copy( (file_to_copy), (path) ); \
	} 

#endif /* _TEST_H_ */
