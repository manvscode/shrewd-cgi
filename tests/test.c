#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <openssl/md5.h>
#include "test.h"
#include "cgi.h"

int main( int argc, char *argv[], char *envp[] ) 
{
	cgi_create( CGI_OPT_ENABLE_LOGGING | CGI_OPT_DISPLAY_ERRORS,
				"photoserver/0.1", 
				"www.test.com"
 	);

	fcgi_begin( );
		cgi_process_request( );
		cgi_header( "Content-Type: text/plain" );
		handle_request( );
		cgi_send_response( );
		cgi_reset( );
	fcgi_end( );

	cgi_destroy( );
	return 0;
}

void handle_request( ) 
{
	cgi_header( "Content-Type: text/plain; charset=utf-8" );
	const char *value = cgi_cookie_get( "last_ts" );
	const char *name  = cgi_cookie_get( "name" );
	const char *dob   = cgi_cookie_get( "dob" );
	const char *id    = cgi_cookie_get( "id" );

	cgi_out( "name = %s\ndob = %s\nid = %s\n", name, dob, id );
	
	time_t ts  = value ? atol( value ) : 0L;
	time_t now = time( NULL );

	if( now >= ts + 30 )
	{
		char last_ts_string[ 32 ];
		snprintf( last_ts_string, sizeof(last_ts_string), "%ld", now );
		cgi_cookie_setf_ex( "last_ts", last_ts_string, NULL );
	}

	cgi_out( "last_ts = %s\n", value );
	cgi_cookie_setf_ex( "dob", "June 15, 1984", NULL );
	cgi_cookie_setf_ex( "id", "3241235", NULL );
	cgi_cookie_setf_ex( "name", "joe", NULL );


	cgi_out( "sz = %ld", sizeof(char *) );
	/*
	for( int i = 0; i < 50; i++ )
	{
		cgi_out( "%d ", i );
	}
	*/

	cgi_out( "\n" );
}


boolean file_exists( const char *path ) 
{
	if( access( path, F_OK ) < 0 ) 
	{
		return FALSE;
	}

	return TRUE;
}

void stream_file( const char *filename ) 
{
	FILE *istream = NULL;
	char buffer[ 4096 ];
	register int i; 

	istream = fopen( filename, "rb" );

	if( !istream )
	{
		ERROR( "Unable to open file." );
	}

	while( !feof( istream ) )
	{
    	size_t bytes_read    = fread( buffer, sizeof(char), sizeof(buffer), istream );

		for( i = 0; i < bytes_read; i++ )
		{
			cgi_out( "%c", buffer[ i ] );
		}
	}

	fclose( istream );
}



