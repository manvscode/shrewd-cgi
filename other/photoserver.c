#include <string.h>
#include <unistd.h>
#include <openssl/md5.h>
#include "photoserver.h"
#include "fapcgi/cgi.h"
#include "simple_image.h"

int main( int argc, char *argv[], char *envp[] ) {
	cgi_create( "photoserver/0.1" );
	simple_image_initialize( NULL, 0 );

	fcgi_begin( );
		cgi_process_request( );
		cgi_header( "Content-Type: text/plain" );
		handle_request( );
		cgi_send_response( );
		cgi_reset( );
	fcgi_end( );

	simple_image_deinitialize( );
	cgi_destroy( );

	return 0;
}

void handle_request( ) {
	const char *mode       = cgi_get( "mode" );	
	const char *token      = cgi_get( "token" );
	const char *hash       = cgi_get( "hash" );
	//const char *username   = cgi_get( "username" );
	//const char *timestamp  = cgi_get( "ts" );

	VERIFY_PARAM( mode );
	VERIFY_PARAM( token );
	VERIFY_PARAM( hash );
	//VERIFY_PARAM( username );
	//VERIFY_PARAM( timestamp );
	
	if( !authenticate( token, hash ) ) {
		//ERROR( "Authentication failed!" );	
	}

	if( strncasecmp( mode, "upload", 6 ) == 0 ) {
		const char *return_url = cgi_get( "return_url" );
		VERIFY_PARAM( return_url );

		handle_upload( token, return_url );
	}
	else if( strncasecmp( mode, "get", 3 ) == 0 ) {
		handle_get( token );
	}
	else if( strncasecmp( mode, "rotate", 6 ) == 0 ) {
		const char *return_url = cgi_get( "return_url" );
		VERIFY_PARAM( return_url );

		handle_rotate( token, return_url );
	}
	else if( strncasecmp( mode, "crop", 4 ) == 0 ) {
		const char *return_url = cgi_get( "return_url" );
		VERIFY_PARAM( return_url );

		handle_crop( token, return_url );
	}
	else if( strncasecmp( mode, "finish", 6 ) == 0 ) {
		const char *return_url = cgi_get( "return_url" );
		VERIFY_PARAM( return_url );

		handle_finish( token, return_url );
	}
	else {
		cgi_error( E_FATAL, "Don't know how to handle mode %s!\n", mode );
	}
}

void handle_upload( const char *token, const char *return_url ) {
	boolean result = TRUE;
	Image *p_image = NULL;

	const cgi_file *p_file = cgi_files( "photo_upload" );
	ASSERT( p_file );

	/* Load the image */
	result = simple_image_load( p_file->tmpname, strlen(p_file->tmpname), &p_image );
	ASSERT( p_image );

	if( !result ) {
		ERROR( "Failed to load the image!" );
		cgi_redirectf( "%s?token=%s&success=%d", return_url, token, result );
		return;
	}

	/* Remove all of the image profiles (EXIF, et cetera) */
	result = simple_image_strip_image( p_image );

	if( !result ) {
		simple_image_destroy( p_image );
		ERROR( "Failed to remove profiles from image!" );
		cgi_redirectf( "%s?token=%s&success=%d", return_url, token, result );
		return;
	}

	/* TODO: Convert to 72 dpi and max size 250 kb */
	p_image->quality = 50; /* This affects the file size */

	/* Save image to uploads directory */
	{
		char original_filename[ 255 ];
		char destination_filename[ 255 ];

		snprintf( original_filename, sizeof(original_filename), "%s/%s_org.jpg", UPLOAD_DIR, token );
		snprintf( destination_filename, sizeof(destination_filename), "%s/%s.jpg", UPLOAD_DIR, token );

		result = simple_image_write( original_filename, strlen(original_filename), p_image );

		if( !result ) {
			simple_image_destroy( p_image );
			ERROR( "Failed to write image to disk!" );
			cgi_redirectf( "%s?token=%s&success=%d", return_url, token, result );
			return;
		}

		if( !copy( original_filename, destination_filename ) ) {
			simple_image_destroy( p_image );
			ERROR( "Failed to copy original image to disk!" );
			cgi_redirectf( "%s?token=%s&success=%d", return_url, token, result );
			return;
		}

	}

	/* Redirect and pass token and success flag */
	{
		ulong width  = simple_image_width( p_image );
		ulong height = simple_image_height( p_image );
		cgi_redirectf( "%s?token=%s&width=%ld&height=%ld&success=%d", return_url, token, width, height, result );
	}

	simple_image_destroy( p_image );
}

/*
 * TODO: Add security. hash = md5 ( paramA + paramB + …. + salt );
 * if hash != hash2 -> fail
 * if ts older than x minutes -> fail
 */
void handle_get( const char *token ) {
	char image_original_filename[ 255 ];
	char image_filename[ 255 ];

	/* Generate image path */
	snprintf( image_original_filename, sizeof(image_original_filename), "%s/%s_org.jpg", UPLOAD_DIR, token );
	snprintf( image_filename, sizeof(image_filename), "%s/%s.jpg", UPLOAD_DIR, token );

	ENSURE_EXISTENCE_AND_COPY( image_filename, image_original_filename ); /* image_filename is guaranteed to exist */

	cgi_header( "Pragma: no-cache" );
	cgi_header( "Cache-Control: no-cache, must-revalidate" ); 
	cgi_header( "Content-Type: image/jpeg" );	
	stream_file( image_filename );
}

void handle_rotate( const char *token, const char *return_url ) {
	boolean result    = TRUE;
	Image *p_image    = NULL;
	const char *angle = cgi_get( "angle" );
	char image_original_filename[ 255 ];
	char image_filename[ 255 ];

	VERIFY_PARAM( angle );

	/* Generate image path */
	snprintf( image_original_filename, sizeof(image_original_filename), "%s/%s_org.jpg", UPLOAD_DIR, token );
	snprintf( image_filename, sizeof(image_filename), "%s/%s.jpg", UPLOAD_DIR, token );

	ENSURE_EXISTENCE_AND_COPY( image_filename, image_original_filename ); /* image_filename is guaranteed to exist */

	/* Load the image */
	result = simple_image_load( image_filename, strlen(image_filename), &p_image );
	ASSERT( p_image );

	if( !result ) {
		ERROR( "Failed to load the image!" );
		cgi_redirectf( "%s?token=%s&success=%d", return_url, token, result );
		return;
	}

	/* Rotate the image */
	{
		double d_angle     = atof( angle );
		Image *p_new_image = NULL;
		result             = simple_image_rotate( p_image, d_angle, &p_new_image );
	
		if( !result ) {
			simple_image_destroy( p_image );
			ERROR( "Failed to rotate image!" );
			cgi_redirectf( "%s?token=%s&success=%d", return_url, token, result );
			return;
		}

		simple_image_destroy( p_image );
		
		p_image = p_new_image;
	}

	/* Save image to uploads directory */
	{
		result = simple_image_write( image_filename, strlen(image_filename), p_image );

		if( !result ) {
			simple_image_destroy( p_image );
			ERROR( "Failed to write image to disk!" );
			cgi_redirectf( "%s?token=%s&success=%d", return_url, token, result );
			return;
		}
	}

	/* Redirect and pass token and success flag */
	{
		ulong width  = simple_image_width( p_image );
		ulong height = simple_image_height( p_image );
		cgi_redirectf( "%s?token=%s&width=%ld&height=%ld&success=%d", return_url, token, width, height, result );
	}
	
	simple_image_destroy( p_image );
}

void handle_crop( const char *token, const char *return_url ) {
	boolean result = TRUE;
	Image *p_image = NULL;
	const char *x1 = cgi_get( "x1" );
	const char *y1 = cgi_get( "y1" );
	const char *x2 = cgi_get( "x2" );
	const char *y2 = cgi_get( "y2" );
	char image_original_filename[ 255 ];
	char image_filename[ 255 ];

	VERIFY_PARAM( x1 );
	VERIFY_PARAM( y1 );
	VERIFY_PARAM( x2 );
	VERIFY_PARAM( y2 );

	/* Generate image path */
	snprintf( image_original_filename, sizeof(image_original_filename), "%s/%s_org.jpg", UPLOAD_DIR, token );
	snprintf( image_filename, sizeof(image_filename), "%s/%s.jpg", UPLOAD_DIR, token );

	ENSURE_EXISTENCE_AND_COPY( image_filename, image_original_filename ); /* image_filename is guaranteed to exist */

	/* Load the image */
	result = simple_image_load( image_filename, strlen(image_filename), &p_image );
	ASSERT( p_image );

	if( !result ) {
		ERROR( "Failed to load the image!" );
		cgi_redirectf( "%s?token=%s&success=%d", return_url, token, result );
		return;
	}

	/* Crop the image */
	{
		long l_x1          = atol( x1 );
		long l_y1          = atol( y1 );
		long l_x2          = atol( x2 );
		long l_y2          = atol( y2 );
		Image *p_new_image = NULL;
		result             = simple_image_crop( p_image, (uint) l_x1, (uint) l_y1, (uint) (l_x2 - l_x1), (uint) (l_y2 - l_y1), &p_new_image );
	
		if( !result ) {
			simple_image_destroy( p_image );
			ERROR( "Failed to crop image!" );
			cgi_redirectf( "%s?token=%s&success=%d", return_url, token, result );
			return;
		}

		simple_image_destroy( p_image );
		
		p_image = p_new_image;
	}

	/* Save image to uploads directory */
	{
		result = simple_image_write( image_filename, strlen(image_filename), p_image );

		if( !result ) {
			simple_image_destroy( p_image );
			ERROR( "Failed to write image to disk!" );
			cgi_redirectf( "%s?token=%s&success=%d", return_url, token, result );
			return;
		}
	}

	/* Redirect and pass token and success flag */
	{
		ulong width  = simple_image_width( p_image );
		ulong height = simple_image_height( p_image );
		cgi_redirectf( "%s?token=%s&width=%ld&height=%ld&success=%d", return_url, token, width, height, result );
	}
	
	simple_image_destroy( p_image );
}

void handle_finish( const char *token, const char *return_url ) {
 	handle_crop( token, return_url );
}



boolean authenticate( const char *token, const char *hash ) {
	MD5_CTX context;
	unsigned char digest[ MD5_DIGEST_LENGTH ];
	char token_and_salt[ 512 ];
	size_t hash_length = strlen( hash );
	char *r;
	int i;

	snprintf( token_and_salt, sizeof(token_and_salt), "%s%s", token, AUTH_SALT );	


	MD5_Init( &context );
	MD5_Update( &context, token_and_salt, strlen(token_and_salt) );
	MD5_Final( digest, &context );

	for( i = 0, r = digest; i < MD5_DIGEST_LENGTH; i++, r += 2 ) {
		sprintf( r, "%02x", digest[ i ] );
	}
	*r = '\0';


	cgi_out( "calc hash = %s\n", r );
	cgi_out( "     hash = %s\n", hash );

	ASSERT( FALSE );

	return strcmp( r, hash ) == 0;
}

boolean copy( const char *src_path, const char *dst_path ) {
	FILE *src_file = NULL;
	FILE *dst_file = NULL;
	char buffer[ 4096 ];

	src_file = fopen( src_path, "rb" );

	if( !src_path ) {
		return FALSE;
	}

	dst_file = fopen( dst_path, "wb" );

	if( !dst_path ) {
		return FALSE;
	}

	while( !feof( src_file ) ) {
    	size_t bytes_read    = fread( buffer, sizeof(char), sizeof(buffer), src_file );
		size_t bytes_written = 0;

		while( bytes_read - bytes_written > 0 ) {
			bytes_written += fwrite( buffer + bytes_written, sizeof(char), bytes_read - bytes_written, dst_file );
		}
	}

	fclose( src_file );
	fclose( dst_file );

	return TRUE;
}

boolean file_exists( const char *path ) {
	if( access( path, F_OK ) < 0 ) {
		return FALSE;
	}

	return TRUE;
}

void stream_file( const char *filename ) {
	FILE *istream = NULL;
	char buffer[ 4096 ];
	register int i; 

	istream = fopen( filename, "rb" );

	if( !istream ) {
		ERROR( "Unable to open file." );
	}

	while( !feof( istream ) ) {
    	size_t bytes_read    = fread( buffer, sizeof(char), sizeof(buffer), istream );

		for( i = 0; i < bytes_read; i++ ) {
			cgi_out( "%c", buffer[ i ] );
		}
	}

	fclose( istream );
}



